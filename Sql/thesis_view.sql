SELECT thesis_id,
       thesis_student_id AS student_id,
       CONCAT(st.user_first_name, ' ', st.user_last_name) AS student_name,
       thesis_client_name AS client_name,
       thesis_client_email AS client_email,
       thesis_primary_supervisor_id AS primary_supervisor_id,
       CONCAT(ps.user_first_name, ' ', ps.user_last_name) AS primary_supervisor_name,
       thesis_secondary_supervisor_id AS secondary_supervisor_id,
       CONCAT(ss.user_first_name, ' ', ss.user_last_name) AS secondary_supervisor_name,
       thesis_topic AS topic,
       thesis_description AS description,
       thesis_approved AS approved,
       thesis_approver_id AS approver_id,
       CONCAT(ed.user_first_name, ' ', ed.user_last_name) AS approver_name,
       thesis_peer_id AS peer_id,
       CONCAT(pr.user_first_name, ' ', pr.user_last_name) AS peer_name,
       thesis_created,
       thesis_last_edit
  FROM thesis
       LEFT JOIN `user` AS st
         ON thesis_student_id = st.user_id
       LEFT JOIN `user` AS ps
         ON thesis_primary_supervisor_id = ps.user_id
       LEFT JOIN `user` AS ss
         ON thesis_secondary_supervisor_id = ss.user_id
       LEFT JOIN `user` AS ed
         ON thesis_approver_id = ed.user_id
   	 LEFT JOIN `user` AS pr
         ON thesis_peer_id = pr.user_id
