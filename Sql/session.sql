CREATE TABLE `session` (
	`session_id` VARCHAR(32) NOT NULL,
	`session_data` TEXT NULL,
	`session_time` INT(10) NOT NULL,
	PRIMARY KEY (`session_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
