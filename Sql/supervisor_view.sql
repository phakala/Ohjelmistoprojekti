SELECT u.user_id,
		 thesis_version_thesis_id AS thesis_id,
		 thesis_version_id AS version_id,
		 thesis_version_title AS version_title,
		 thesis_version_ready AS version_ready,
		 thesis_version_estimated_date AS version_estimated_date
  FROM thesis_version
  		 LEFT JOIN thesis AS t
  		 	ON t.thesis_id = thesis_version_thesis_id
  		 LEFT JOIN `user` AS u
  		 	ON u.user_id = t.thesis_student_id
