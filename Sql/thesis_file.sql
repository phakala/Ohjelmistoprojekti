CREATE TABLE `thesis_file` (
	`thesis_file_thesis_id` INT(11) NOT NULL,
	`thesis_file_version` VARCHAR(64) NOT NULL,
	`thesis_file_path` VARCHAR(255) NOT NULL,
	`thesis_file_estimated_completion` DATETIME NOT NULL,
	`thesis_file_completed` INT(11) NOT NULL,
	PRIMARY KEY (`thesis_file_thesis_id`),
	CONSTRAINT `fk_thesis_id` FOREIGN KEY (`thesis_file_thesis_id`) REFERENCES `thesis` (`thesis_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
