CREATE TABLE `thesis` (
	`thesis_id` INT(11) NOT NULL AUTO_INCREMENT,
	`thesis_student_id` VARCHAR(7) NOT NULL,
	`thesis_client_name` VARCHAR(255) NULL DEFAULT NULL,
	`thesis_client_email` VARCHAR(255) NULL DEFAULT NULL,
	`thesis_primary_supervisor_id` VARCHAR(7) NULL DEFAULT NULL,
	`thesis_secondary_supervisor_id` VARCHAR(7) NULL DEFAULT NULL,
	`thesis_topic` VARCHAR(128) NULL DEFAULT NULL,
	`thesis_description` MEDIUMTEXT NULL,
	`thesis_approved` INT(11) NULL DEFAULT '0',
	`thesis_created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
	`thesis_last_edit` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`thesis_id`),
	UNIQUE INDEX `thesis_student_id` (`thesis_student_id`),
	INDEX `fk_thesis_user_idx` (`thesis_student_id`),
	INDEX `fk_thesis_user1_idx` (`thesis_primary_supervisor_id`),
	INDEX `fk_thesis_user2_idx` (`thesis_secondary_supervisor_id`),
	CONSTRAINT `fk_thesis_user` FOREIGN KEY (`thesis_student_id`) REFERENCES `user` (`user_id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `fk_thesis_user1` FOREIGN KEY (`thesis_primary_supervisor_id`) REFERENCES `user` (`user_id`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `fk_thesis_user2` FOREIGN KEY (`thesis_secondary_supervisor_id`) REFERENCES `user` (`user_id`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=175
;
