CREATE TABLE `user` (
	`user_id` VARCHAR(7) NOT NULL,
	`user_first_name` VARCHAR(50) NULL DEFAULT NULL,
	`user_last_name` VARCHAR(50) NULL DEFAULT NULL,
	`user_email` VARCHAR(255) NULL DEFAULT NULL,
	`user_roles` INT(11) NULL DEFAULT NULL,
	`user_primary_supervisions` INT(11) NULL DEFAULT '0',
	`user_secondary_supervisions` INT(11) NULL DEFAULT '0',
	`user_first_login` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`user_id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
