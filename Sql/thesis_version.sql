CREATE TABLE `thesis_version` (
	`thesis_version_id` INT(11) NOT NULL AUTO_INCREMENT,
	`thesis_version_thesis_id` INT(11) NOT NULL,
	`thesis_version_title` VARCHAR(255) NULL DEFAULT NULL,
	`thesis_version_comment` TEXT NULL,
	`thesis_version_ready` INT(11) NOT NULL DEFAULT '0',
	`thesis_version_estimated_date` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`thesis_version_thesis_id`, `thesis_version_id`),
	INDEX `thesis_version_id` (`thesis_version_id`),
	CONSTRAINT `fk_version_thesis_id` FOREIGN KEY (`thesis_version_thesis_id`) REFERENCES `thesis` (`thesis_id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=15
;
