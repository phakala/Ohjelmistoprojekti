<?php
namespace ThesisManager\Controller;
use ThesisManager\Controller;
use ThesisManager\Library\Template;
use ThesisManager\Model\MenuItems;
use ThesisManager\Model\Theses;

/**
 *  Review page
 */
class Review extends Controller
{
    public function Form($args) {
		$review = $args[0];
		if ($review == 1) {
			$theses = new Theses;
			$thesis = $theses->GetByStudentId($_SESSION["User"]["LabraId"]);
		}
		if ($review == 2) {
			$theses = new Theses;
			$thesis = $theses->GetPeer($_SESSION["User"]["LabraId"]);
		}
		if ($review == 3) {
			$theses = new Theses;
			$thesis = $theses->GetByStudentId($_POST["studentid"]);
		}
        $view = new Template("Page/Base.phtml");
		$view->leftMenuItems = MenuItems::GenerateLeftMenu();
		$view->burgerMenuItems = MenuItems::GenerateBurgerMenu();
		$content = new Template("Partial/Review/Form.phtml");
		$content->studentId = $thesis->StudentId();
		$content->studentName = $thesis->StudentName();
		$content->topic = $thesis->Topic();
        $view->content = $content->ToString();
		$view->Display();
    }
	public function Summary() {
		$view = new Template("Page/Base.phtml");
		$form = new Template("Partial/Review/Summary.phtml");
		$view->leftMenuItems = MenuItems::GenerateLeftMenu();
		$view->burgerMenuItems = MenuItems::GenerateBurgerMenu();
        $view->content = $form->ToString();
        $view->Display();
	}
	public function Save() {
		$data = array();
		$data[0] = $_POST["comment"];
		$data[1] = $_POST["average"];
		$theses = new Theses;
		$thesis = $theses->GetByStudentId($_POST["authorid"]);
		$data[2] = $thesis->Id();
		$review = $theses->SaveReview($data);
		if ($review) {
			$view = new Template("Page/Base.phtml");
			$form = new Template("Partial/Review/Success.phtml");
			$view->leftMenuItems = MenuItems::GenerateLeftMenu();
			$view->burgerMenuItems = MenuItems::GenerateBurgerMenu();
        	$view->content = $form->ToString();
        	$view->Display();
		}
		else {
			$view = new Template("Page/Base.phtml");
			$form = new Template("Partial/Review/Error.phtml");
			$view->leftMenuItems = MenuItems::GenerateLeftMenu();
			$view->burgerMenuItems = MenuItems::GenerateBurgerMenu();
        	$view->content = $form->ToString();
        	$view->Display();
		}


	}


}
