<?php
namespace ThesisManager\Controller;
use ThesisManager\Controller;
use ThesisManager\Constant\Environment;
use ThesisManager\Constant\UserRole;
use ThesisManager\Library\Template;
use ThesisManager\Model\MenuItems;

/**
 *  Home controller.
 */
class Home extends Controller
{
    public function Home() {
        // User is logged out
        if (!isset($_SESSION["User"])) {
            $view = new Template("Page/Index");
			$view->devForm = "";
            // Add development login form when in development environment.
            if (APP_ENV == Environment::Development) {
				$form = new Template("Partial/Login/Development");
                $view->devForm = $form->ToString();
			}
			$form = new Template("Partial/Login/Production");
			$view->loginForm = $form->ToString();
            $view->Display();
        }
        // User is logged in
        else {
			// Default pages shown for roles at domain root.
			if ($_SESSION["User"]["Roles"] & UserRole::Admin) {
				$this->Forward("Admin", "Users");
			}
			else if ($_SESSION["User"]["Roles"] & UserRole::Student) {
				$this->Forward("Student", "Thesis");
			}
            else if ($_SESSION["User"]["Roles"] & UserRole::ThesisSupervisor) {
				$this->Forward("ThesisSupervisor", "Theses");
			}
			else if ($_SESSION["User"]["Roles"] & UserRole::LanguageInstructor) {
				$this->Forward("LanguageSupervisor", "Theses");
			}
            $view = new Template("Page/Base");
            $view->leftMenuItems = MenuItems::GenerateLeftMenu();
			$view->burgerMenuItems = MenuItems::GenerateBurgerMenu();
			$view->content = "";
            $view->Display();
        }
    }
}

