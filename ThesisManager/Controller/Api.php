<?php
namespace ThesisManager\Controller;
use ThesisManager\Controller;
use ThesisManager\Constant\UserRole;
use ThesisManager\Config\ThesisFiles;
use ThesisManager\Model\User;
use ThesisManager\Model\Users;
use ThesisManager\Model\Thesis;
use ThesisManager\Model\Theses;
use ThesisManager\Model\ThesisVersion;
use ThesisManager\Model\ThesisVersions;

/**
 *  Public API calls.
 */
class Api extends Controller
{
    public function Users($args)
    {
		if ($this->CheckCsrfToken()) {
			if (isset($_SESSION["User"])) {
				$json = [];
				$users = new Users;
				$action = isset($args[0]) ? $args[0] : NULL;
				$object = isset($args[1]) ? $args[1] : NULL;
				// TODO rights for other roles that can fetch users.
				switch($action) {
					case "get":
						switch ($object) {
							case "supervisors":
								if ($_SESSION["User"]["Roles"] & UserRole::EducationCoordinator) {
									// TODO add more fields if needed.
									foreach ($users->GetThesisSupervisors() as $supervisor) {
										$json[] = [
											"labraId" => $supervisor["user_id"],
											"name" => $supervisor["user_name"],
											"currentPrimarySupervisions" => $supervisor["current_primary_supervisions"] ?? 0,
											"currentSecondarySupervisions" => $supervisor["current_secondary_supervisions"] ?? 0,
											"maxPrimarySupervisions" => $supervisor["max_primary_supervisions"] ?? 0,
											"maxSecondarySupervisions" => $supervisor["max_secondary_supervisions"] ?? 0,
											"notAnOptionPrimary" => ($supervisor["current_primary_supervisions"] < $supervisor["max_primary_supervisions"] && $supervisor["max_primary_supervisions"] > 0) ? false : true,
											"notAnOptionSecondary" => ($supervisor["current_secondary_supervisions"] < $supervisor["max_secondary_supervisions"] && $supervisor["max_secondary_supervisions"] > 0) ? false : true
										];
									}
								}
								break 2;
							default:
								if ($_SESSION["User"]["Roles"] & UserRole::Admin) {
									foreach ($users->GetAll() as $user) {
										$json[] = [
											"labraId" => $user["user_id"],
											"firstName" => $user["user_first_name"],
											"lastName" => $user["user_last_name"],
											"email" => $user["user_email"],
											"roles" => [
												UserRole::Admin => [
													"name" => _("label_admin"),
													"set" => $user["user_roles"] & UserRole::Admin ? true : false
												],
												UserRole::Student => [
													"name" => _("label_student"),
													"set" => $user["user_roles"] & UserRole::Student ? true : false
												],
												UserRole::ThesisSupervisor => [
													"name" => _("label_thesis_supervisor"),
													"set" => $user["user_roles"] & UserRole::ThesisSupervisor ? true : false
												],
												UserRole::LanguageInstructor => [
													"name" => _("label_language_instructor"),
													"set" => $user["user_roles"] & UserRole::LanguageInstructor ? true : false
												],
												UserRole::EducationCoordinator => [
													"name" => _("label_education_coordinator"),
													"set" => $user["user_roles"] & UserRole::EducationCoordinator ? true : false
												],
												UserRole::EducationDirector => [
													"name" => _("label_education_director"),
													"set" => $user["user_roles"] & UserRole::EducationDirector ? true : false
												]
											]
										];
									}
								}
								break 2;
						}
					case "save":
						if ($_SESSION["User"]["Roles"] & UserRole::Admin ||
						    $_SESSION["User"]["Roles"] & UserRole::EducationCoordinator) {
							try {
								// Get POST data from received JSON object.
								$data = $this->GetJsonPostData();

								// TODO Permissions by role per column.

								// User / LabraNet ID
								$labraId = isset($data["user"]["labraId"])
										   ? $data["user"]["labraId"]
										   : $_SESSION["User"]["LabraId"];

								// First name
								$firstName = isset($data["user"]["firstName"])
											 ? $data["user"]["firstName"]
											 : NULL;

								// Last name
								$lastName = isset($data["user"]["lastName"])
											? $data["user"]["lastName"]
											: NULL;

								// Email address
								$email = isset($data["user"]["email"])
										 ? $data["user"]["email"]
										 : NULL;

								// Roles
								$roles = isset($data["user"]["roles"]) ? 0 : NULL;
								if (!empty($roles)) {
									foreach ($roles as $key => $value) {
										if ($value["set"]) $roles += $key;
									}
								}

								$primarySupervisions = isset($data["user"]["maxPrimarySupervisions"])
													   ? $data["user"]["maxPrimarySupervisions"]
													   : NULL;

								$secondarySupervisions 	= isset($data["user"]["maxSecondarySupervisions"])
														  ? $data["user"]["maxSecondarySupervisions"]
														  : NULL;

								$user = new User($labraId, $firstName, $lastName, $email, $roles, $primarySupervisions, $secondarySupervisions);
								$user->Save();
								http_response_code(200);
								break;
							}
							catch (\Exception $e) {
								// TODO errors
								$json["error"]["general"] = true;
							}
						}
					default:
						http_response_code(400);
						return;
				}
				header("Content-type: application/json");
				echo $this->SafeJson($json);
				return;
			}
		}
        $this->DisplayNotFound();
    }

	public function Theses($args)
    {
		if ($this->CheckCsrfToken()) {
			if (isset($_SESSION["User"])) {
				$json = []; // Response object.
				$theses = new Theses;
				$action = isset($args[0]) ? $args[0] : NULL;
				$object = isset($args[1]) ? $args[1] : NULL;
				switch($action) {
					case "get":
						switch ($object) {
							default:
								if ($_SESSION["User"]["Roles"] & UserRole::Admin ||
									$_SESSION["User"]["Roles"] & UserRole::EducationCoordinator)
								{
									foreach ($theses->GetAll() as $thesis) {
										$json[] = [
											"thesisId" => $thesis["thesis_id"],
											"studentId" => $thesis["student_id"],
											"studentName" => $thesis["student_name"],
											"clientName" => $thesis["client_name"],
											"clientEmail" => $thesis["client_email"],
											"primarySupervisorId" => $thesis["primary_supervisor_id"],
											"primarySupervisorName" => $thesis["primary_supervisor_name"] ?? "-",
											"secondarySupervisorId" => $thesis["secondary_supervisor_id"],
											"secondarySupervisorName" => $thesis["secondary_supervisor_name"] ?? "-",
											"startDate" => $thesis["thesis_created"],
											"lastEdit" => $thesis["thesis_last_edit"],
											"topic" => $thesis["topic"],
											"description" => $thesis["description"],
											"approved" => (bool)$thesis["approved"],
											"approver" => $thesis["approver_id"]
										];
									}
								}
								break 2;
						}
					case "save":
						// Roles that can use this API.
                		if ($_SESSION["User"]["Roles"] & UserRole::Admin ||
                			$_SESSION["User"]["Roles"] & UserRole::Student ||
                		    $_SESSION["User"]["Roles"] & UserRole::EducationCoordinator)
						{
							// Try to save thesis in the database.
							try {
								// Get POST data from received JSON object.
								$data = $this->GetJsonPostData();

								// Permissions by role per column.

								// Thesis ID
								// Admin (probably shouldn't even by admin?)
								$thesisId = (($_SESSION["User"]["Roles"] & UserRole::Admin)
											&& isset($data["thesis"]["thesisId"]))
											? $data["thesis"]["thesisId"]
											: NULL;

								// Student ID
								// Admin and education coordinator can save by student ID.
								// Also needs to be set when a student creates/updates a thesis.
								// Note: Damn PHP's left associativity
								$studentId = (($_SESSION["User"]["Roles"] & UserRole::Admin || $_SESSION["User"]["Roles"] & UserRole::EducationCoordinator)
											 && isset($data["thesis"]["studentId"]))
											 ? $data["thesis"]["studentId"]
											 : ($_SESSION["User"]["Roles"] & UserRole::Student // and students can also save by student ID which cannot be changed (Session variable).
											 ? $_SESSION["User"]["LabraId"]
											 : NULL);

								// Client name
								$clientName = (($_SESSION["User"]["Roles"] & UserRole::Admin ||
											    $_SESSION["User"]["Roles"] & UserRole::Student) // Admin & Student can change client name.
											  && isset($data["thesis"]["clientName"]))
											  ? $data["thesis"]["clientName"]
											  : NULL;

								// Client email
								$clientEmail = (($_SESSION["User"]["Roles"] & UserRole::Admin ||
											     $_SESSION["User"]["Roles"] & UserRole::Student) // Admin & student can change client email.
											   && isset($data["thesis"]["clientEmail"]))
											   ? $data["thesis"]["clientEmail"]
											   : NULL;

								// Thesis topic
								$topic = (($_SESSION["User"]["Roles"] & UserRole::Admin ||
										   $_SESSION["User"]["Roles"] & UserRole::Student) // Admin and student can set the thesis topic.
										 && isset($data["thesis"]["topic"]))
										 ? $data["thesis"]["topic"]
										 : NULL;

								// Thesis description
								$description = (($_SESSION["User"]["Roles"] & UserRole::Admin ||
												 $_SESSION["User"]["Roles"] & UserRole::Student) // Admin and student can set the thesis description
											   && isset($data["thesis"]["description"]))
											   ? $data["thesis"]["description"]
											   : NULL;

								// Primary supervisor
								$primarySupervisorId = (($_SESSION["User"]["Roles"] & UserRole::Admin || // Admin and education coordinator can set a 1st supervisor.
													 $_SESSION["User"]["Roles"] & UserRole::EducationCoordinator)
												   && isset($data["thesis"]["primarySupervisorId"]))
												   ? $data["thesis"]["primarySupervisorId"]
												   : NULL;

								// Secondary supervisor
								$secondarySupervisorId = (($_SESSION["User"]["Roles"] & UserRole::Admin || // Admin and education coordinator can set a 2nd supervisor.
													  $_SESSION["User"]["Roles"] & UserRole::EducationCoordinator)
													&& isset($data["thesis"]["secondarySupervisorId"]))
												    ? $data["thesis"]["secondarySupervisorId"]
												    : NULL;

								// Thesis approval
								$approved = (($_SESSION["User"]["Roles"] & UserRole::EducationCoordinator) // Education coordinator can approve theses.
											&& isset($data["thesis"]["approved"]))
											? (int)$data["thesis"]["approved"]
											: NULL;

								// Save approver if approval was set
								$approverId = $approved ? $_SESSION["User"]["LabraId"] : NULL;

								// Thesis peer
								$peerId = (($_SESSION["User"]["Roles"] & UserRole::EducationCoordinator) // Education coordinator can set the peer reviewer.
										  && isset($data["thesis"]["peerId"]))
										  ? $data["thesis"]["peerId"]
										  : NULL;

								$thesis = new Thesis(
												$thesisId, $studentId, NULL,
												$clientName, $clientEmail,
												$primarySupervisorId, NULL,
												$secondarySupervisorId, NULL,
												$topic, $description,
												$approved, $approverId,
												$peerId, NULL);
								$thesis->Save();
								http_response_code(200);
								break;
							}
							catch (\Exception $e) {
								// TODO errors
								$json["error"]["general"] = true;
							}
						}
					default:
						http_response_code(400);
						break;
				}
				header("Content-type: application/json");
            	echo $this->SafeJson($json);
            	return;
			}
		}
        $this->DisplayNotFound();
    }


	public function Versions($args)
    {
		if ($this->CheckCsrfToken()) {
			if (isset($_SESSION["User"])) {
				$json = []; // Response object.
				$thesisVersions = new ThesisVersions;
				$action = isset($args[0]) ? $args[0] : NULL;
				$object = isset($args[1]) ? $args[1] : NULL;
				switch($action) {
					case "get":
						switch ($object) {
							default:
								if ($_SESSION["User"]["Roles"] & UserRole::Student)
								{
									foreach ($thesisVersions->GetByStudentId() as $version)
									{
										$json[] = [
											"versionId" => $version["version_id"],
											"title" => $version["version_title"],
											"comment" => $version["version_comment"],
											"estimatedDate" => $version["version_estimated_date"],
											"ready" => $version["version_ready"],
											"files" => [
												["name" => "asdgfd.docx"],
												["name" => "fggdf.pdf"]
											]
										];
									}
								}
								break 2;
						}
					case "save":
						switch($object) {
							// Thesis version saving
							case "version":
								// Roles that can use this API.
								if ($_SESSION["User"]["Roles"] & UserRole::Admin ||
									$_SESSION["User"]["Roles"] & UserRole::Student)
								{
									//if (!isset($data["version"])) break 2;
									// Try to save the version in the database.
									try {
										// Get POST data from received JSON object.
										$data = $this->GetJsonPostData();

										// TODO Permissions by role per column.

										$versionId = isset($data["version"]["versionId"])
													 ? $data["version"]["versionId"]
													 : 0;

										$thesisId = isset($data["version"]["thesisId"])
													? $data["version"]["thesisId"]
													: $_SESSION["User"]["ThesisId"];

										// Version title
										$title = isset($data["version"]["title"])
												 ? $data["version"]["title"]
												 : NULL;

										// Version title
										$comment = isset($data["version"]["comment"])
												   ? $data["version"]["comment"]
												   : NULL;

										// Ready for review
										$ready = isset($data["version"]["ready"])
												 ? $data["version"]["ready"]
												 : NULL;

										// Estimated completion date.
										$date = isset($data["version"]["estimatedDate"])
												? \DateTime::createFromFormat('m/Y', $data["version"]["estimatedDate"])->format("Y-m-d H:i:s")
												: NULL;

										$version = new ThesisVersion($versionId, $thesisId, $title, $comment, $ready, $date);
										$version->Save();
										http_response_code(200);
										break 2;
									}
									catch (\Exception $e) {
										// TODO errors
										$json["error"]["general"] = true;
									}
								}
								break 2;
							default:
								break 2;
						}
					default:
						http_response_code(400);
						break;
				}
				header("Content-type: application/json");
            	echo $this->SafeJson($json);
            	return;
			}
		}
        $this->DisplayNotFound();
    }


	/*
	 *	Thesis files API calls
	 */
	public function Files($args)
	{
		if ($this->CheckCsrfToken()) {
			if (isset($_SESSION["User"])) {
				$action = isset($args[0]) ? $args[0] : NULL;
				$json = [];
				switch($action)
				{
					case "upload":
						if (!empty($_FILES["files"]) && !empty($_POST["versionId"])) {
							$files = $this->sortFileArray($_FILES["files"]);
							$fileInfo = new \finfo(\FILEINFO_MIME_TYPE);
							$uploadedFiles = [];
							foreach($files as $file) {
								// TODO file validation etc.
								if ($ext = array_search($fileInfo->file($file["tmp_name"]), ThesisFiles::AllowedTypes, true));
								{
									$fileName = sprintf("%s.%s", \sha1_file($file["tmp_name"]), $ext);
									if (move_uploaded_file($file["tmp_name"], ThesisFiles::SavePath . $fileName))
									{
										$uploadedFiles[] = [
											"versionId" => $_POST["versionId"],
											"fileName" => $file["name"],
											"filePath" => $fileName
										];
										$json[] = [
											"name" => $file["name"]
										];
									}
								}
							}
							$thesisVersion = new ThesisVersion($_POST["versionId"], $_SESSION["User"]["ThesisId"]);
							$thesisVersion->AddFiles($uploadedFiles);
						}
						http_response_code(200);
						break;
					default:
						http_response_code(400);
						break;
				}
				header("Content-type: application/json");
            	echo $this->SafeJson($json);
				return;
			}
		}
		$this->DisplayNotFound();
	}

	private function sortFileArray(&$files) {
		$array = [];
		$count = count($files["name"]);
		$keys = array_keys($files);
		for ($i = 0; $i < $count; $i++) {
			foreach($keys as $key) {
				$array[$i][$key] = $files[$key][$i];
			}
		}
		return $array;
	}

}
