<?php
namespace ThesisManager\Controller;
use ThesisManager\Controller;
use ThesisManager\Constant\Environment;
use ThesisManager\Model\User as UserModel;
use ThesisManager\Model\Users;
use ThesisManager\Library\Template;
use ThesisManager\Traits\MenuItems;

/**
 *  User controller.
 */
class User extends Controller
{
    /**
     *  Form input, type: POST
     *  Validate user input and try to log in.
     */
    public function Login() {
		$users = new Users;
		// TODO error messages
		// LabraNet login.
		if (!empty($_POST["labraid"]) && !empty($_POST["password"])) {
			$labraId = htmlspecialchars(trim($_POST["labraid"]));
			$password = htmlspecialchars(trim($_POST["password"]));
			if ($user = $users->GetByLabraNetLogin($labraId, $password)) {
            	$_SESSION["User"]["LabraId"] = $user->LabraId();
  				$_SESSION["User"]["FirstName"] = $user->FirstName();
				$_SESSION["User"]["LastName"] = $user->LastName();
				$_SESSION["User"]["Email"] = $user->Email();
				$_SESSION["User"]["Roles"] = $user->Roles();
			}
		}
        // Development environment login.
        else if (APP_ENV == Environment::Development) {
            $user = new UserModel($_POST["labraid"], $_POST["labraid"], "Tester", "{$_POST["labraid"]}@tester.com", $_POST["devRole"]);
			$user->Save();
            $_SESSION["User"]["LabraId"] = $user->LabraId();
			$_SESSION["User"]["FirstName"] = $user->FirstName();
			$_SESSION["User"]["LastName"] = $user->LastName();
			$_SESSION["User"]["Email"] = $user->Email();
			$_SESSION["User"]["Roles"] = $user->Roles();
        }
        else {
            // TODO error handling
        }
		header("Location: /");
    }

    /**
     *  Destroys the session and redirects user to the front page.
     */
	public function Logout() {
		session_destroy();
		header("Location: /");
	}
}



