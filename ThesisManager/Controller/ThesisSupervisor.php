<?php
namespace ThesisManager\Controller;
use ThesisManager\Controller;
use ThesisManager\Library\Template;
use ThesisManager\Constant\UserRole;
use ThesisManager\Model\MenuItems;
use ThesisManager\Model\Theses;
use ThesisManager\Model\ThesisVersion;
use ThesisManager\Model\ThesisVersions;

/**
 *  Thesis supervisor controller.
 */
class ThesisSupervisor extends Controller
{
    public function Theses() {
		if (isset($_SESSION["User"])) {
            if ($_SESSION["User"]["Roles"] & UserRole::ThesisSupervisor) {
                $view = new Template("Page/Base");
                $view->leftMenuItems = MenuItems::GenerateLeftMenu();
				$view->burgerMenuItems = MenuItems::GenerateBurgerMenu();
				$theses = new Theses;
				$students1 = $theses->GetPrimaryStudents($_SESSION["User"]["LabraId"]);
				$students2 = $theses->GetSecondaryStudents($_SESSION["User"]["LabraId"]);
				$content = new Template("Partial/ThesisSupervisor/ThesisSupervisor.phtml");
				$content->students1 = $students1;
				$content->students2 = $students2;
				$view->content = $content->ToString();
                $view->Display();
                return;
            }
        }

        $this->DisplayNotFound();
    }
    public function Student($args) {
		if (isset($_SESSION["User"])) {
            if ($_SESSION["User"]["Roles"] & UserRole::ThesisSupervisor) {
                $studentid = $args[0];
                $view = new Template("Page/Base");
                $view->leftMenuItems = MenuItems::GenerateLeftMenu();
				$view->burgerMenuItems = MenuItems::GenerateBurgerMenu();
				$theses = new Theses;
				$thesis = $theses->GetByStudentId($studentid);
				$thesisid = $thesis->Id();
				$reviews = $theses->GetReviews($thesisid);
				$content = new Template("Partial/ThesisSupervisor/Student.phtml");
				$content->studentId = $thesis->StudentId();
				$content->studentName = $thesis->StudentName();
				$content->clientName = $thesis->ClientName();
				$content->clientEmail = $thesis->ClientEmail();
				$content->topic = $thesis->Topic();
				$content->description = $thesis->Description();
				$content->primarySupervisor = $thesis->PrimarySupervisorName();
				$content->secondarySupervisor = $thesis->SecondarySupervisorName();
				$content->dates = $theses->GetDates();
				$content->reviews = $reviews;
				$content->thesisid = $thesisid;
				$content->approver = $thesis->ApproverName();
				$content->peer = $thesis->PeerName();
				$versions = new Thesisversions;
				$versiondivs = $versions->GenerateVersionBoxes($studentid);
				$content->versions = $versiondivs;
                $view->content = $content->ToString();
                $view->Display();
                return;
            }
        }
        $this->DisplayNotFound();
}
}
