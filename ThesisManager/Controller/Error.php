<?php
namespace ThesisManager\Controller;
use ThesisManager\Controller;
use ThesisManager\Library\Template;

/**
 *  Error handling.
 */
class Error extends Controller
{
    public function NotFound() {
        http_response_code(404);
        $view = new Template("Page/404");
        $view->Display();
    }

    public function Error() {
        $view = new Template("Page/Error");
        $view->Display();
    }
}
