<?php
namespace ThesisManager\Controller;
use ThesisManager\Controller;
use ThesisManager\Constant\Environment;
use ThesisManager\Constant\UserRole;
use ThesisManager\Model\MenuItems;
use ThesisManager\Model\User as UserModel;
use ThesisManager\Model\Users;
use ThesisManager\Library\Template;


/**
 *  Education coordinator controller.
 */
class EducationCoordinator extends Controller
{
    /**
     *  Theses management page
     */
    public function Theses() {
        if (isset($_SESSION["User"])) {
            if ($_SESSION["User"]["Roles"] & UserRole::EducationCoordinator) {
                $view = new Template("Page/Base");
                $view->leftMenuItems = MenuItems::GenerateLeftMenu();
				$view->burgerMenuItems = MenuItems::GenerateBurgerMenu();
                $userTable = new Template("Partial/EducationCoordinator/Theses.phtml");
                $view->content = $userTable->ToString();
                $view->Display();
                return;
            }
        }
        $this->DisplayNotFound();
    }

	/**
     *  Thesis tutors management page
     */
    public function ThesisSupervisors() {
        if (isset($_SESSION["User"])) {
            if ($_SESSION["User"]["Roles"] & UserRole::EducationCoordinator) {
                $view = new Template("Page/Base");
                $view->leftMenuItems = MenuItems::GenerateLeftMenu();
				$view->burgerMenuItems = MenuItems::GenerateBurgerMenu();
                $userTable = new Template("Partial/EducationCoordinator/ThesisSupervisors.phtml");
                $view->content = $userTable->ToString();
                $view->Display();
                return;
            }
        }
        $this->DisplayNotFound();
    }
}



