<?php
namespace ThesisManager\Controller;
use ThesisManager\Controller;
use ThesisManager\Constant\Environment;
use ThesisManager\Constant\UserRole;
use ThesisManager\Model\User as UserModel;
use ThesisManager\Model\Users;
use ThesisManager\Library\Template;
use ThesisManager\Model\MenuItems;

/**
 *  Admin controller.
 */
class Admin extends Controller
{
    /**
     *  User management page
     */
    public function Users() {
        if (isset($_SESSION["User"])) {
            if ($_SESSION["User"]["Roles"] & UserRole::Admin) {
                $view = new Template("Page/Base");
                $view->leftMenuItems = MenuItems::GenerateLeftMenu();
                $view->burgerMenuItems = MenuItems::GenerateBurgerMenu();
                $userTable = new Template("Partial/Admin/Users");
                $view->content = $userTable->ToString();
                $view->Display();
                return;
            }
        }
        $this->DisplayNotFound();
    }
}



