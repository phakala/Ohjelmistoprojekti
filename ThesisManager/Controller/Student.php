<?php
namespace ThesisManager\Controller;
use ThesisManager\Controller;
use ThesisManager\Library\Template;
use ThesisManager\Constant\UserRole;
use ThesisManager\Model\MenuItems;
use ThesisManager\Model\Theses;

/**
 *  Student controller.
 */
class Student extends Controller
{
    public function Thesis() {
		if (isset($_SESSION["User"])) {
            if ($_SESSION["User"]["Roles"] & UserRole::Student) {
                $view = new Template("Page/Base");
                $view->leftMenuItems = MenuItems::GenerateLeftMenu();
				$view->burgerMenuItems = MenuItems::GenerateBurgerMenu();
				$theses = new Theses;
				$thesis = $theses->GetByStudentId($_SESSION["User"]["LabraId"]);
				if ($thesis) $_SESSION["User"]["ThesisId"] = $thesis->Id();
				if (!$thesis || !$thesis->Approved()) {
					// Thesis topic not yet approved
					$content = new Template("Partial/Student/Pending.phtml");
					$content->studentId = $_SESSION["User"]["LabraId"];
					$content->studentName = $_SESSION["User"]["FirstName"] ." ". $_SESSION["User"]["LastName"];
					$content->studentEmail = $_SESSION["User"]["Email"];
					$content->clientName = $thesis ? $thesis->ClientName() : "";
					$content->clientEmail = $thesis ? $thesis->ClientEmail() : "";
					$content->topic = $thesis ? $thesis->Topic() : "";
					$content->description = $thesis ? $thesis->Description() : "";
                	$view->content = $content->ToString();
				} else if ($thesis->Approved()) {
					// Thesis approved, but not completed.
					$content = new Template("Partial/Student/Making.phtml");
					$content->studentId = $thesis->StudentId();
					$content->studentName = $thesis->StudentName();
					$content->clientName = $thesis->ClientName();
					$content->clientEmail = $thesis->ClientEmail();
					$content->topic = $thesis->Topic();
					$content->description = $thesis->Description();
					$content->primarySupervisor = $thesis->PrimarySupervisorName();
					$content->secondarySupervisor = $thesis->SecondarySupervisorName();
					$content->approver = $thesis->ApproverName();
					$content->peer = $thesis->PeerName();
					$content->dates = $theses->GetDates();
                	$view->content = $content->ToString();
				} else {
					$view->content = "";
				}
                $view->Display();
                return;
            }
        }
        $this->DisplayNotFound();
    }
	public function Peer() {
		if (isset($_SESSION["User"])) {
            if ($_SESSION["User"]["Roles"] & UserRole::Student) {
                $view = new Template("Page/Base");
                $view->leftMenuItems = MenuItems::GenerateLeftMenu();
				$view->burgerMenuItems = MenuItems::GenerateBurgerMenu();
				$theses = new Theses;
				$thesis = $theses->GetPeer($_SESSION["User"]["LabraId"]);
				if ($thesis) {
					$studentid = $thesis->StudentId();
					$thesis = $theses->GetByStudentId($studentid);
					$content = new Template("Partial/Student/Peer.phtml");
					$content->studentId = $thesis->StudentId();
					$content->studentName = $thesis->StudentName();
					$content->clientName = $thesis->ClientName();
					$content->clientEmail = $thesis->ClientEmail();
					$content->topic = $thesis->Topic();
					$content->description = $thesis->Description();
					$content->primarySupervisor = $thesis->PrimarySupervisorName();
					$content->secondarySupervisor = $thesis->SecondarySupervisorName();
					$content->dates = $theses->GetDates();
                	$view->content = $content->ToString();
                	$view->Display();
                	return;
				}
				else {
					$view->content = "";
				}
            }
        }
        $this->DisplayNotFound();
    }

}


