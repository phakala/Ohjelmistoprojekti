<?php
namespace ThesisManager\Controller;
use ThesisManager\Controller;

/**
 *  Locale controller.
 */
class Locale extends Controller
{
    public function Change() {
        if (isset($_POST["language"])) {
            $locales = require ROOT_PATH . "/ThesisManager/Config/Locales.php";
            if (in_array($_POST["language"], $locales)) {
                $_SESSION["Locale"] = $_POST["language"];
                if (isset($_GET["r"])) {
                    header("Location: {$_GET["r"]}");
                    return;
                }
                header("Location: /");
                return;
            }
        }
        $this->DisplayNotFound();
    }
}

