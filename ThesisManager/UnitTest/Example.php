<?php
require "../Library/Testing.php";

class Example extends Test
{
    public function ExampleTest() {
        Assert::AreEqual(1, 2);
    }

    public function ExampleTestTwo() {
        Assert::AreEqual(2, 2);
    }

    public function ExampleTestThree() {
        Assert::AreEqual(false, 0);
    }

    public function ExampleTestFour() {
        Assert::AreEqual(true, true);
    }

    public function ExampleTestFive() {
        Assert::AreEqual("10", 10);
    }

    public function ExampleTestSix() {
        Assert::AreEqual((int)true, 1);
    }

    public function ExampleTestSeven() {
        Assert::AreEqual(false, (bool)0);
    }
}
