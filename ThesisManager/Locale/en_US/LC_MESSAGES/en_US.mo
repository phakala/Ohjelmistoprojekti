��    Y      �     �      �  
   �     �     �     �     �     �     �       	        (     =     I     ^     m     z     �     �     �     �  
   �     �     �     	     #	     3	     O	     h	     t	     �	     �	     �	     �	  
   �	     �	     �	     �	     
     
     .
     B
     U
  
   e
  	   p
     z
     �
  
   �
     �
     �
     �
     �
     �
          %     3     @     P     `     n     z     �     �     �     �     �     �               0     C     R     _     s     �     �     �     �     �     �               1  
   =     H     T     b     v     �     �  �  �     G     K     R     X     ]     f     k  w  r  	   �     �          	          !     (     5     A     I     ^     n     s          �  	   �     �     �     �     �  	   �               -  #   0     T     `     o     �     �     �     �  	   �     �     �     �     �     �     �     �     �               1     D     L  	   S  	   ]     g     o  
   u     �     �     �     �     �     �     �  
   �     �           	          %     2     9     I     P     b     t     �     �     �     �     �     �     �     �     �  O   �     L   T              O                              =   ?                     W   3   :           E   &   '       *   H         <       8   P      
       B       1                	                  X      F      S   C         >         /   9       %   0   6          7                         K   I           #       @   (       M       )       D   Q   Y   2   4   $   U   R   !       +      .   N       "          A   J           ,   5              -   V   ;              G    button_add button_cancel button_login button_next button_previous button_save button_submit description_thesis_topic error_404 error_page_not_found label_admin label_administration label_approved label_author label_client_email label_client_name label_comment label_create_new_version label_current_average label_date label_description label_edit_supervisions label_edit_thesis label_edit_user label_education_coordinator label_education_director label_email label_estimated_date label_file_name label_files label_form_error label_id label_jamk label_labraid label_labranet_login label_language_instructor label_logout label_manage_supervisions label_manage_theses label_manage_users label_my_thesis label_name label_new label_overall_average label_password label_peer label_peer_review label_per_page label_previous label_primary_students label_primary_supervisions label_primary_supervisor label_results label_return label_review_by label_reviewing label_reviews label_roles label_save_error label_save_success label_score label_secondary_students label_secondary_supervisions label_secondary_supervisor label_self_evaluation label_student label_student_id label_student_name label_students label_submit label_submit_review label_supervisions label_theses label_thesis_approver label_thesis_client label_thesis_management label_thesis_suggestion label_thesis_supervisor label_thesis_supervisors label_thesis_title label_topic label_user label_users label_version label_version_ready label_versions label_weighted_grade text_thesis_approved Project-Id-Version: ThesisManager
POT-Creation-Date: 2017-04-25 23:39+0300
PO-Revision-Date: 2017-04-25 23:42+0300
Last-Translator: 
Language-Team: 
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.1
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Add Cancel Login Next Previous Save Submit All theses are made as commissions. Many students find a thesis topic during their practical training. You should consider potential thesis topics during your practical training and offer them to the company. Do not expect a ready-made topic to fall into your lap – be active in your search for topics. The programme coordinator and teacher tutor can help you find a topic. Error 404 Page not found Admin Administration Approved Author Client Email Client Name Comment Create a new version Current Average Date Description Set supervision limits Edit Thesis Edit User Education Coordinator Education Director Email Address Estimated completion date File name Files Please fill out the entire form ID JAMK University of Applied Sciences LabraNet ID LabraNet Login Language Instructor Logout Manage supervisions Manage theses Manage users My Thesis Name New Overall average Password Peer Peer Review Per page Previous Primary supervisees Primary supervisions Primary supervisor Results Return Review by Reviewing Reviews Roles Save error Success Score Secondary supervisees Secondary supervisions Secondary supervisor Self Evaluation Student Student ID Student Name Students Continue Lähetä arviointi Supervisions Theses Thesis approver Client Thesis Management Thesis Suggestion Thesis Supervisor Thesis supervisors Title Topic User Users Version Version ready Versions Weighted grade Your thesis topic suggestion has been approved by %s. You can now upload files. 