��    \      �     �      �  
   �     �     �     �               '     5  	   N     X     m     y     �     �     �     �     �     �     �  
   	     	     )	     A	     S	     c	     	     �	     �	     �	     �	     �	     �	     �	     
  
   
     
     (
     =
     W
     d
     ~
     �
     �
  
   �
  	   �
     �
     �
  
   �
     �
               *     A     \     u     �     �     �     �     �     �     �     �     �          0     K     a     o     �     �     �     �     �     �     �     �          !     9     Q     i     �     �  
   �     �     �     �     �     �     �  �       �     �     �     �  	   �     �     �  }  �  	   Z     d     w  	   �     �     �     �     �  	   �     �     �     �               &     @     V     g     |  #   �     �  	   �  $   �     �     �               -     9     P     `     n     �     �     �     �     �  	   �     �     �     �       	   
          "     /     D     M     S  	   \  
   f     q  0   x     �     �     �  
   �     �     �  
   �               (     4     :  0   C  	   t     ~     �     �     �     �     �     �                    $     1     8     F     N  L   d     O   W              R                              ?   A                     Z   5   <           G   (   )       ,   J         >       :   S      
       D       3                	              "   [      H      V   E         @         1   ;       '   2   8          9                         N   L           %   \   B   *       P       +       F   T   !   4   6   &   X   U   #       -      0   Q       $          C   M   K       .   7              /   Y   =              I    button_add button_cancel button_login button_next button_previous button_save button_submit description_thesis_topic error_404 error_page_not_found label_admin label_administration label_approved label_author label_client_email label_client_name label_comment label_create_new_version label_current_average label_date label_description label_edit_supervisions label_edit_thesis label_edit_user label_education_coordinator label_education_director label_email label_estimated_date label_file_name label_files label_form_error label_frontpage label_go_review label_id label_jamk label_labraid label_labranet_login label_language_instructor label_logout label_manage_supervisions label_manage_theses label_manage_users label_my_thesis label_name label_new label_overall_average label_password label_peer label_peer_review label_per_page label_previous label_primary_students label_primary_supervisions label_primary_supervisor label_results label_return label_review_by label_reviewing label_reviews label_roles label_save_error label_save_success label_score label_secondary_students label_secondary_supervisions label_secondary_supervisor label_self_evaluation label_student label_student_id label_student_name label_students label_submit label_submit_review label_summary_error label_supervisions label_theses label_thesis_approver label_thesis_client label_thesis_management label_thesis_suggestion label_thesis_supervisor label_thesis_supervisors label_thesis_title label_topic label_user label_users label_version label_version_ready label_versions label_weighted_grade text_thesis_approved Project-Id-Version: 
POT-Creation-Date: 2017-04-25 23:42+0300
PO-Revision-Date: 2017-04-25 23:45+0300
Last-Translator: 
Language-Team: 
Language: fi_FI
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.1
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Lisää Peruuta Kirjaudu Seuraava Edellinen Tallenna Lähetä Kaikki opinnäytetyöt tehdään toimeksiantoina. Moni opiskelija löytää opinnäytetyön aiheen harjoitteluaikana. Mieti siis harjoittelussa ollessasi mahdollisia opinnäytetyön aiheita ja tarjoa niitä yritykselle. Älä odota, että saat jostakin valmiin aiheen - ole aktiivinen ja etsi itse aiheita. Koulutusvastaava ja opettajatutor voivat auttaa sinua aiheen etsinnässä. Virhe 404 Sivua ei löytynyt Ylläpitäjä Ylläpito Hyväksytty Tekijä Asiakkaan Sähköposti Asiakkaan Nimi Kommentti Luo uusi versio Nykyinen keskiarvo Päivämäärä Kuvaus Aseta enimmäisohjaukset Muokkaa opinnäytetyötä Muokkaa käyttäjää Koulutusvastaava Koulutuspäällikkö Sähköposti Arvioitu valmistumispäivämäärä Tiedoston nimi Tiedostot Täytä koko lomake ennen jatkamista Takaisin etusivulle Arvioi Tunnus Jyväskylän ammattikorkeakoulu LabraNet ID LabraNet kirjautuminen Kielten Ohjaaja Kirjaudu ulos Hallitse ohjauksia Hallinnoi opinnäytetöitä Hallinnoi käyttäjiä Opinnäytetyö Nimi Uusi Keskiarvo Salasana Vertaisarvioija Vertaisarviointi Per sivu Edellinen 1. Ohjattavat 1. ohjaukset Ensimmäinen ohjaaja Tulokset Palaa Arvioija Arviointi Arvioinnit Roolit Jotain meni pieleen, yritä myöhemmin uudelleen Arviointi tallennettu Tulos 2. Ohjattavat 2. ohjaaja Toinen ohjaaja Itsearviointi Opiskelija Opiskelijatunnus Opiskelijan nimi Opiskelijat Jatka Tallenna Jotain meni pieleen, yritä myöhemmin uudelleen Ohjaukset Opinnäytetyöt Aiheen hyväksyjä Tilaaja Opinnäytetyön hallinta Opinnäytetyön ehdotus Opinnäytetyön Ohjaaja Opinnäytetyön ohjaajat Otsikko Aihe Käyttäjä Käyttäjät Versio Versio valmis Versiot (painotettu arvosana) Opinnäytetyösi aihe-ehdotus on hyväksynyt %s. Voit nyt ladata tiedostoja. 