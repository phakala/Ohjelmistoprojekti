<?php
namespace ThesisManager\Model;
use ThesisManager\Model;
use ThesisManager\Config\ThesisDB;
use ThesisManager\Library\Database;
use ThesisManager\Constant\UserRole;

/**
 *  User model.
 */
class User extends Model {
    private $labraId;
	private $firstName;
	private $lastName;
	private $email;
	private $roles;
	private $primarySupervisions;
	private $secondarySupervisions;

    function __construct(
		$labraId,
		$firstName = NULL,
		$lastName = NULL,
		$email = NULL,
		$roles = NULL,
		$primarySupervisions = NULL,
		$secondarySupervisions = NULL)
	{
        parent::__construct(new Database(ThesisDB::Host, ThesisDB::User, ThesisDB::Password, ThesisDB::Schema));
        $this->labraId = $labraId;
		$this->firstName = $firstName;
		$this->lastName = $lastName;
		$this->email = $email;
		$this->roles = $roles;
		$this->primarySupervisions = $primarySupervisions;
		$this->secondarySupervisions = $secondarySupervisions;
	}

    public function LabraId() {
        return $this->labraId;
    }

	public function FirstName() {
		return $this->firstName;
	}

	public function LastName() {
		return $this->lastName;
	}

	public function Email() {
		return $this->email;
	}

	public function Roles() {
        return $this->roles;
    }

	public function PrimarySupervisions() {
		return $this->primarySupervisions;
	}

	public function SecondarySupervisions() {
		return $this->secondarySupervisions;
	}

    /*
     *  Saves the user in the database.
     */
    public function Save() {
        try {
            $this->db->Query("
                INSERT INTO `user` (user_id, user_first_name, user_last_name, user_email, user_roles, user_primary_supervisions, user_secondary_supervisions)
                     VALUES (:id, :firstName, :lastName, :email, :roles, :primarySupervisions, :secondarySupervisions)
                         ON DUPLICATE KEY
                     UPDATE user_first_name = COALESCE(:firstName2, user_first_name),
					 		user_last_name = COALESCE(:lastName2, user_last_name),
							user_email = COALESCE(:email2, user_email),
							user_roles = COALESCE(:roles2, user_roles),
							user_primary_supervisions = COALESCE(:primarySupervisions2, user_primary_supervisions),
							user_secondary_supervisions = COALESCE(:secondarySupervisions2, user_secondary_supervisions)
            ");
            $this->db->Bind(":id", $this->labraId);
            $this->db->Bind(":firstName", $this->firstName);
            $this->db->Bind(":firstName2", $this->firstName);
            $this->db->Bind(":lastName", $this->lastName);
            $this->db->Bind(":lastName2", $this->lastName);
            $this->db->Bind(":email", $this->email);
            $this->db->Bind(":email2", $this->email);
            $this->db->Bind(":roles", $this->roles);
            $this->db->Bind(":roles2", $this->roles);
            $this->db->Bind(":primarySupervisions", $this->primarySupervisions);
            $this->db->Bind(":primarySupervisions2", $this->primarySupervisions);
            $this->db->Bind(":secondarySupervisions", $this->secondarySupervisions);
            $this->db->Bind(":secondarySupervisions2", $this->secondarySupervisions);
            $this->db->Execute();
            return true;
        }
        catch (\Exception $e) {
            // TODO error handling
			error_log($e->GetMessage());
            return false;
        }
    }
}





