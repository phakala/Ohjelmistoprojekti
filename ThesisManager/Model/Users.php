<?php
namespace ThesisManager\Model;
use ThesisManager\Model;
use ThesisManager\Model\User;
use ThesisManager\Config\Ldap;
use ThesisManager\Config\ThesisDB;
use ThesisManager\Constant\UserRole;
use ThesisManager\Library\Database;

/**
 *  Contains methods for handling users.
 */
class Users extends Model {
    function __construct() {
		parent::__construct(new Database(ThesisDB::Host, ThesisDB::User, ThesisDB::Password, ThesisDB::Schema));
	}

    /**
     *  Get an user by LabraNet login.
     */
    public function GetByLabraNetLogin($labraId, $password) {
        try {
            $connection = ldap_connect(Ldap::Host, Ldap::Port);
            ldap_set_option($connection, LDAP_OPT_PROTOCOL_VERSION, 3); // Use LDAP protocol version 3.
            ldap_set_option($connection, LDAP_OPT_REFERRALS, 0); // Disable LDAP referrals.
            ldap_set_option($connection, LDAP_OPT_X_TLS_REQUIRE_CERT, 0); // Disable check for LabraNet's self-signed? certificate.
            ldap_start_tls($connection); // Use secure connection.
            if ($bind = ldap_bind($connection, $labraId.'@'.Ldap::Domain, $password)) {
				$filter = "(sAMAccountName={$labraId})";
    			$attributes = ["givenname", "sn", "mail"]; // Search for name and email.
    			$result = ldap_search($connection, Ldap::DN, $filter, $attributes);
    			$entries = ldap_get_entries($connection, $result);
    			$firstName = $entries[0]["givenname"][0] ?? NULL;
				$lastName = $entries[0]["sn"][0] ?? NULL;
				$email = $entries[0]["mail"][0] ?? NULL;
				$this->db->Query("
					SELECT *
					  FROM `user`
					 WHERE user_id = :id
				");
				$this->db->Bind(":id", $labraId);
				if ($row = $this->db->Single()) {
					$roles = $row["user_roles"];
					$primarySupervisions = $row["user_primary_supervisions"];
					$secondarySupervisions = $row["user_secondary_supervisions"];
				}
				else {
					$roles = UserRole::Student; // Default user role.
					$primarySupervisions = 0;
					$secondarySupervisions = 0;
				}
				$user = new User($labraId, $firstName, $lastName, $email, $roles, $primarySupervisions, $secondarySupervisions);
				if (!$row) $user->Save(); // Save new user to the database.
                return $user;
            }
        }
        catch (\Exception $e) {
            // TODO error handling
        }
        return false;
    }

	/**
     *  Get an user by LabraNet ID
     */
    public function GetByLabraId($labraId) {
        try {
            $this->db->Query("
                SELECT *
                  FROM `user`
				 WHERE user_id = :id
            ");
			$this->db->Bind(":id", $labraId);
            return $this->db->Single();
        }
        catch (\Exception $e) {
            // TODO error handling
        }
        return false;
    }

    /**
     *  Get all users from the database.
     */
    public function GetAll() {
        try {
            $this->db->Query("
                SELECT *
                  FROM `user`
            ");
            return $this->db->All();
        }
        catch (\Exception $e) {
            // TODO error handling
        }
        return false;
    }

	/**
     *  Get all thesis supervisors from the database.
     */
    public function GetThesisSupervisors() {
        try {
            $this->db->Query("
                SELECT *
                  FROM supervisor_view
            ");
            return $this->db->All();
        }
        catch (\Exception $e) {
            // TODO error handling
        }
        return false;
    }
}



