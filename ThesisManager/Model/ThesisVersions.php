<?php
namespace ThesisManager\Model;
use ThesisManager\Model;
use ThesisManager\Model\User;
use ThesisManager\Model\Version;
use ThesisManager\Config\ThesisDB;
use ThesisManager\Library\Database;

/**
 *  Contains methods for handling thesis versions.
 */
class ThesisVersions extends Model {
    function __construct() {
		parent::__construct(new Database(ThesisDB::Host, ThesisDB::User, ThesisDB::Password, ThesisDB::Schema));
	}

	/**
	 *	Get thesis versions by Student (LabraNet) ID.
	 */
	public function GetByStudentId($labraId = NULL) {
		$labraId = $labraId ?? $_SESSION["User"]["LabraId"];
		try {
			$this->db->Query("
				SELECT *
				  FROM thesis_version_view
				 WHERE student_id = :id
			");
			$this->db->Bind(":id", $labraId);
			return $this->db->All();
		}
		catch (\Exception $e) {
			// TODO error handling
			return false;
		}
	}

	//gets thesis versions for a student and generates divs for director - For demo purposes only !!
	public function GenerateVersionBoxes($id) {
		try {
			$this->db->Query("
				SELECT *
				  FROM thesis_version_view
				 WHERE student_id = :id
			");
			$divs = "";
			$this->db->Bind(":id", $id);
			foreach ($this->db->All() as $row) {
                $divs .= "<div class='thesis-version box'><p>" . $row["version_title"] . "</p></div>";
			}
			return $divs;
		}
		catch (\Exception $e) {
			// TODO error handling
			return false;
		}
	}

}



