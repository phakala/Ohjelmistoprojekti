<?php
namespace ThesisManager\Model;
use ThesisManager\Model;
use ThesisManager\Config\ThesisDB;
use ThesisManager\Library\Database;

/**
 *  Thesis version model.
 */
class ThesisVersion extends Model {
	private $versionId;
    private $thesisId;
	private $title;
	private $comment;
	private $ready;
	private $estimatedDate;

    function __construct(
		$versionId,
		$thesisId,
		$title = NULL,
		$comment  = NULL,
		$ready = NULL,
		$estimatedDate = NULL
	)
	{
        parent::__construct(new Database(ThesisDB::Host, ThesisDB::User, ThesisDB::Password, ThesisDB::Schema));
		$this->versionId = $versionId;
		$this->thesisId = $thesisId;
		$this->title = $title;
		$this->comment = $comment;
		$this->ready = $ready;
		$this->estimatedDate = $estimatedDate;
	}

	public function VersionId() {
		return $this->versionId;
	}

    public function ThesisId() {
        return $this->thesisId;
    }

	public function Title() {
        return $this->title;
    }

	public function Comment() {
        return $this->comment;
    }

	public function Ready() {
        return $this->ready;
    }

	public function EstimatedDate() {
        return $this->estimatedDate;
    }

	/*
	 *	Saves the object into the database.
	 */
	public function Save() {
		try {
			$this->db->Query("
                INSERT INTO `thesis_version` (thesis_version_id, thesis_version_thesis_id, thesis_version_title, thesis_version_comment, thesis_version_ready, thesis_version_estimated_date)
                     VALUES (:versionId, :thesisId, :title, :comment, :ready, :date)
                         ON DUPLICATE KEY
                     UPDATE thesis_version_title = COALESCE(:title2, thesis_version_title),
                     		thesis_version_comment = COALESCE(:comment2, thesis_version_comment),
							thesis_version_ready = COALESCE(:ready2, thesis_version_ready),
							thesis_version_estimated_date = COALESCE(:date2, thesis_version_estimated_date)
            ");
			$this->db->Bind(":versionId", $this->versionId);
			$this->db->Bind(":thesisId", $this->thesisId);
			$this->db->Bind(":title", $this->title);
			$this->db->Bind(":title2", $this->title);
			$this->db->Bind(":comment", $this->comment);
			$this->db->Bind(":comment2", $this->comment);
			$this->db->Bind(":ready", $this->ready);
			$this->db->Bind(":ready2", $this->ready);
			$this->db->Bind(":date", $this->estimatedDate);
			$this->db->Bind(":date2", $this->estimatedDate);
			$this->db->Execute();
			return true;
		}
		catch (\Exception $e) {
			// TODO error handling
			error_log($e->GetMessage());
			return false;
		}
	}

	/*
	 *	Links files to a version
	 */
	public function AddFiles($files) {
		try {
			$this->db->BeginTransaction();
			foreach($files as $file) {
				$this->db->Query("
					INSERT INTO thesis_file
						 VALUES (:id, :name, :path)
				");
				$this->db->Bind(":id", $this->versionId);
				$this->db->Bind(":name", $file["fileName"]);
				$this->db->Bind(":path", $file["filePath"]);
				$this->db->Execute();
			}
			$this->db->EndTransaction();
		}
		catch (\Exception $e) { }
	}
}





