<?php
namespace ThesisManager\Model;
use ThesisManager\Model;
use ThesisManager\Model\User;
use ThesisManager\Model\Thesis;
use ThesisManager\Config\ThesisDB;
use ThesisManager\Library\Database;

/**
 *  Contains methods for handling users.
 */
class Theses extends Model {
    function __construct() {
		parent::__construct(new Database(ThesisDB::Host, ThesisDB::User, ThesisDB::Password, ThesisDB::Schema));
	}

	/**
	 *	Get a thesis by Student (LabraNet) ID.
	 */
	public function GetByStudentId($labraId) {
		try {
			$this->db->Query("
				SELECT *
				  FROM thesis_view
				 WHERE student_id = :id
			");
			$this->db->Bind(":id", $labraId);
			if ($row = $this->db->Single()) {
				return new Thesis(
					$row["thesis_id"],
					$row["student_id"],
					$row["student_name"],
					$row["client_name"],
					$row["client_email"],
					$row["primary_supervisor_id"],
					$row["primary_supervisor_name"],
					$row["secondary_supervisor_id"],
					$row["secondary_supervisor_name"],
					$row["topic"],
					$row["description"],
					$row["approved"],
					$row["approver_id"],
					$row["approver_name"],
					$row["peer_id"],
					$row["peer_name"]
				);
			}
		}
		catch (\Exception $e) {
			// TODO error handling
			return false;
		}
	}

    /**
     *  Get all theses from the database.
     */
    public function GetAll() {
        try {
            $this->db->Query("
                SELECT *
                  FROM thesis_view
            ");
            return $this->db->All();
        }
        catch (\Exception $e) {
            // TODO error handling
			return false;
        }
    }

	public function GetDates() {
		$date = date("Y-m-01");
		$string = "";
		for ($i=0; $i<10; $i++) {
			$newdate = strtotime ( '+' . $i . ' month' , strtotime ( $date ) ) ;
			$string .= "<option value='" . date('m/Y',$newdate) . "'>" . date('m/Y',$newdate) . "</option>";
		}
		return $string;
	}

	public function GetPrimaryStudents($labraId) {
		try {
			$query = $this->db->Query("
				SELECT *
				  FROM thesis_view
				 WHERE primary_supervisor_id = :id
			");
            $links = "";
			$this->db->Bind(":id", $labraId);
			foreach ($this->db->All() as $row) {
                $links .= "<a href='/supervisor/student/" . $row["student_id"] . "'>" . $row["student_id"] . " " . $row["student_name"] . "</a><br>";
			}
			return $links;
		}
		catch (\Exception $e) {
			// TODO error handling
			return false;
		}
	}

	public function GetSecondaryStudents($labraId) {
		try {
			$query = $this->db->Query("
				SELECT *
				  FROM thesis_view
				 WHERE secondary_supervisor_id = :id
			");
            $links = "";
			$this->db->Bind(":id", $labraId);
			foreach ($this->db->All() as $row) {
                $links .= "<a href='/supervisor/student/" . $row["student_id"] . "'>" . $row["student_id"] . " " . $row["student_name"] . "</a><br>";
			}
			return $links;
		}
		catch (\Exception $e) {
			// TODO error handling
			return false;
		}
	}

		public function GetPeer($labraId) {
		try {
			$this->db->Query("
				SELECT *
				  FROM thesis_view
				 WHERE peer_id = :id
			");
			$this->db->Bind(":id", $labraId);
			if ($row = $this->db->Single()) {
				return new Thesis(
					$row["thesis_id"],
					$row["student_id"],
					$row["student_name"],
					$row["client_name"],
					$row["client_email"],
					$row["primary_supervisor_id"],
					$row["primary_supervisor_name"],
					$row["secondary_supervisor_id"],
					$row["secondary_supervisor_name"],
					$row["topic"],
					$row["description"],
					$row["approved"],
					$row["approver_id"],
					$row["approver_name"],
					$row["peer_id"],
					$row["peer_name"]
				);
			}
		}
		catch (\Exception $e) {
			// TODO error handling
			return false;
		}
	}

	public function SaveReview($data) {
		$reviewer = $_SESSION["User"]["LabraId"];
		try {
			$this->db->Query("
				INSERT INTO review
				(review_reviewer, review_thesis, review_score, review_comment)
				VALUES (:reviewer, :thesisid, :score, :comment)
			");
			$this->db->Bind(":reviewer", $reviewer);
			$this->db->Bind(":thesisid", $data[2]);
			$this->db->Bind(":score", $data[1]);
			$this->db->Bind(":comment", $data[0]);
			$this->db->Execute();
			return true;
		}
		catch (\Exception $e) {
			// TODO error handling
			return false;
		}
	}

	public function GetReviews($id) {
		try {
			$this->db->Query("
				SELECT *
				  FROM review_view
				 WHERE thesis = :id
			");
			$this->db->Bind(":id", $id);
			$reviews = "";
			foreach ($this->db->All() as $row) {
                $reviews .= '<h1> ' . _("label_review_by") . ' : ' . $row["reviewer_name"] . '</h1><h2>' . _("label_score") . ' : ' . $row["score"] . '</h2><h2>' . _("label_comment") . ' : ' . $row["comments"] . '</h2><br><br>';
			}
			return $reviews;
		}
		catch (\Exception $e) {
			// TODO error handling
			return false;
		}
	}

	public function GetAllStudents() {
		try {
			$query = $this->db->Query("
				SELECT *
				  FROM thesis_view
			");
            $links = "";
			foreach ($this->db->All() as $row) {
                $links .= "<a href='/language/student/" . $row["student_id"] . "'>" . $row["student_id"] . " " . $row["student_name"] . "</a><br>";
			}
			return $links;
		}
		catch (\Exception $e) {
			// TODO error handling
			return false;
		}
	}

}



