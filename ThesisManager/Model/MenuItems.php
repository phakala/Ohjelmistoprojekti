<?php
namespace ThesisManager\Model;

/**
 *  Contains methods for generating menu links
 */
class MenuItems {
	/**
	 *	Generate left menu.
	 */
	public static function GenerateLeftMenu() {
		$items = require ROOT_PATH . "/ThesisManager/Config/MenuLinks.php";
		$html = "";
		$path = $_GET["path"] ?? NULL;
		foreach ($items as $role => $item) {
			if (!is_numeric($role) || $_SESSION["User"]["Roles"] & $role) {
				$html .= "<p class=\"menu-label\">{$item[0]}</p>";
				$html .= "<ul class=\"menu-list\">";
				foreach ($item[1] as $link => $name) {
					$active = ($path === $link) ? "class=\"is-active\"" : "";
					$html .= "<li><a href=\"/{$link}\" {$active}>{$name}</a></li>";
				}
				$html .= "</ul>";
			}
		}
		return $html;
	}

	/**
	 *	Generate burger menu
	 */
	public static function GenerateBurgerMenu() {
		$items = require ROOT_PATH . "/ThesisManager/Config/MenuLinks.php";
		$html = "";
		$path = $_GET["path"] ?? NULL;
		foreach ($items as $role => $item) {
			if (!is_numeric($role) || $_SESSION["User"]["Roles"] & $role) {
				foreach ($item[1] as $link => $name) {
					$active = ($path === $link) ? " is-active" : "";
					$html .= "<a href=\"/{$link}\" class=\"nav-item{$active}\">{$item[0]} - {$name}</a>";
				}
			}
		}
		return $html;
	}

}



