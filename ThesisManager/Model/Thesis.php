<?php
namespace ThesisManager\Model;
use ThesisManager\Model;
use ThesisManager\Config\ThesisDB;
use ThesisManager\Library\Database;

/**
 *  Thesis model.
 */
class Thesis extends Model {
    private $id;
	private $studentId;
	private $studentName;
	private $clientName;
	private $clientEmail;
	private $primarySupervisorId;
	private $primarySupervisorName;
	private $secondarySupervisorId;
	private $secondarySupervisorName;
	private $topic;
	private $description;
	private $approved;
	private $peer;

    function __construct(
		$id = NULL,
		$studentId = NULL,
		$studentName = NULL,
		$clientName = NULL,
		$clientEmail = NULL,
		$primarySupervisorId = NULL,
		$primarySupervisorName = NULL,
		$secondarySupervisorId = NULL,
		$secondarySupervisorName = NULL,
		$topic = NULL,
		$description = NULL,
		$approved = NULL,
		$approverId = NULL,
		$approverName = NULL,
		$peerId = NULL,
		$peerName = NULL
	)
	{
        parent::__construct(new Database(ThesisDB::Host, ThesisDB::User, ThesisDB::Password, ThesisDB::Schema));
		$this->id = $id;
		$this->studentId = $studentId;
		$this->studentName = $studentName;
		$this->clientName = $clientName;
		$this->clientEmail = $clientEmail;
		$this->primarySupervisorId = $primarySupervisorId;
		$this->primarySupervisorName = $primarySupervisorName;
		$this->secondarySupervisorId = $secondarySupervisorId;
		$this->secondarySupervisorName = $secondarySupervisorName;
		$this->topic = $topic;
		$this->description = $description;
		$this->approved = $approved;
		$this->approverId = $approverId;
		$this->approverName = $approverName;
		$this->peerId = $peerId;
		$this->peerName = $peerName;
	}

    public function Id() {
        return $this->id;
    }

	public function StudentId() {
        return $this->studentId;
    }

	public function StudentName() {
        return $this->studentName;
    }

	public function ClientName() {
        return $this->clientName;
    }

	public function ClientEmail() {
        return $this->clientEmail;
    }

	public function PrimarySupervisorId() {
        return $this->primarySupervisorId;
    }

	public function PrimarySupervisorName() {
        return $this->primarySupervisorName;
    }

	public function SecondarySupervisorId() {
        return $this->secondarySupervisorId;
    }

	public function SecondarySupervisorName() {
        return $this->secondarySupervisorName;
    }

	public function Topic() {
        return $this->topic;
    }

	public function Description() {
        return $this->description;
    }

	public function Approved() {
        return $this->approved;
    }

	public function ApproverId() {
        return $this->approverId;
    }

	public function ApproverName() {
        return $this->approverName;
    }


	public function PeerId() {
        return $this->peerId;
    }

	public function PeerName() {
        return $this->peerName;
    }

	/*
	 *	Saves the object into the database.
	 */
	public function Save() {
		try {
			$this->db->Query("
                INSERT INTO `thesis` (thesis_student_id,
									  thesis_client_name, thesis_client_email,
									  thesis_primary_supervisor_id, thesis_secondary_supervisor_id,
									  thesis_topic, thesis_description,
									  thesis_approved, thesis_approver_id,
									  thesis_peer_id)
                     VALUES (:studentId, :clientName, :clientEmail, :primarySupervisorId, :secondarySupervisorId, :topic, :description, :approved, :approverId, :peerId)
                         ON DUPLICATE KEY
                     UPDATE thesis_client_name = COALESCE(:clientName2, thesis_client_name),
							thesis_client_email = COALESCE(:clientEmail2, thesis_client_email),
							thesis_primary_supervisor_id = COALESCE(:primarySupervisorId2, thesis_primary_supervisor_id),
							thesis_secondary_supervisor_id = COALESCE(:secondarySupervisorId2, thesis_secondary_supervisor_id),
							thesis_topic = COALESCE(:topic2, thesis_topic),
							thesis_description = COALESCE(:description2, thesis_description),
							thesis_approved = COALESCE(:approved2, thesis_approved),
							thesis_approver_id = COALESCE(:approverId2, thesis_approver_id),
							thesis_peer_id = COALESCE(:peerId2, thesis_peer_id)
            ");
			$this->db->Bind(":studentId", $this->studentId);
			$this->db->Bind(":clientName", $this->clientName);
			$this->db->Bind(":clientName2", $this->clientName);
			$this->db->Bind(":clientEmail", $this->clientEmail);
			$this->db->Bind(":clientEmail2", $this->clientEmail);
			$this->db->Bind(":primarySupervisorId", $this->primarySupervisorId);
			$this->db->Bind(":primarySupervisorId2", $this->primarySupervisorId);
			$this->db->Bind(":secondarySupervisorId", $this->secondarySupervisorId);
			$this->db->Bind(":secondarySupervisorId2", $this->secondarySupervisorId);
			$this->db->Bind(":topic", $this->topic);
			$this->db->Bind(":topic2", $this->topic);
			$this->db->Bind(":description", $this->description);
			$this->db->Bind(":description2", $this->description);
			$this->db->Bind(":approved", $this->approved);
			$this->db->Bind(":approved2", $this->approved);
			$this->db->Bind(":approverId", $this->approverId);
			$this->db->Bind(":approverId2", $this->approverId);
			$this->db->Bind(":peerId", $this->peer);
			$this->db->Bind(":peerId2", $this->peer);
			$this->db->Execute();
			return true;
		}
		catch (\Exception $e) {
			// TODO error handling
			error_log($e);
			return false;
		}
	}
}





