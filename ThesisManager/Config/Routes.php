<?php

/**
 *	Controller and action route rules.
 *  Format: Regex => [Controller, Action]
 *  Example: "/^controller\/action\/?$/" => ["Controller", "Action"]
 *  With ID: "/^controller\/action\/(\d{1,4})?\/?$/" => ["Controller", "Action"],
 */

return [
    // Admin
    "/^admin\/users\/?$/" => ["Admin", "Users"],

    // API
	"/^api\/files\/([a-z]+)\/?([a-zA-Z0-9]+)?\/?$/" => ["Api", "Files"],
	"/^api\/theses\/([a-z]+)\/?([a-zA-Z0-9]+)?\/?$/" => ["Api", "Theses"],
    "/^api\/users\/([a-z]+)\/?([a-zA-Z0-9]+)?\/?$/" => ["Api", "Users"],
	"/^api\/versions\/([a-z]+)\/?([a-zA-Z0-9]+)?\/?$/" => ["Api", "Versions"],

	// Education coordinator
	"/^coordinator\/theses\/?$/" => ["EducationCoordinator", "Theses"],
	"/^coordinator\/supervisors\/?$/" => ["EducationCoordinator", "ThesisSupervisors"],

	//Language supervisor
	"/^language\/theses\/?$/" => ["LanguageSupervisor", "Theses"],
	"/^language\/student\/?([a-zA-Z0-9]+)?\/?$/" => ["LanguageSupervisor", "Student"],

    // Index
    "/^\/$/" => ["Home", "Home"],

    // Language change
    "/^lang\/?$/" => ["Locale", "Change"],

    // Student
    "/^student\/thesis\/?$/" => ["Student", "Thesis"],
	"/^student\/peer\/?$/" => ["Student", "Peer"],

	// Thesis supervisor
    "/^supervisor\/theses\/?$/" => ["ThesisSupervisor", "Theses"],
    "/^supervisor\/student\/?([a-zA-Z0-9]+)?\/?$/" => ["ThesisSupervisor", "Student"],


    // User
    "/^user\/login\/?$/" => ["User", "Login"],
    "/^user\/logout\/?$/" => ["User", "Logout"],

	//review
	"/^review\/form\/?([a-zA-Z0-9]+)?\/?$/" => ["Review", "Form"],
	"/^review\/summary\/?$/" => ["Review", "Summary"],
	"/^review\/save\/?$/" => ["Review", "Save"],

    // 404 everything else
    "/^(.*)$/" => ["Error", "NotFound"]
];
