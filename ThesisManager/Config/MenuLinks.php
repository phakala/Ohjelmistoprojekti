<?php
use ThesisManager\Constant\UserRole;

/**
 *	Menu items
 */
return [
	UserRole::Admin => [
		_("label_administration"),
		[
			"admin/users/" => _("label_users"),
			"admin/theses/" => _("label_theses")
		]
	],
	UserRole::Student => [
		_("label_student"),
		[
			"student/thesis/" => _("label_my_thesis"),
			"student/peer/" => _("label_peer_review"),
			"review/form/1" => _("label_self_evaluation")
		]
	],
	UserRole::ThesisSupervisor => [
		_("label_thesis_supervisor"),
		[
			"supervisor/theses/" => _("label_theses")
		]
	],
	UserRole::LanguageInstructor => [
		_("label_language_instructor"),
		[
			"language/theses/" => _("label_theses")
		]
	],
	UserRole::EducationCoordinator => [
		_("label_education_coordinator"),
		[
			"coordinator/theses/" => _("label_theses"),
			"coordinator/supervisors" => _("label_thesis_supervisors")
		]
	],
	UserRole::EducationDirector => [
		_("label_education_director"),
		[
			"#" => "todo"
		]
	],
	"User" => [
		_("label_user"),
		[
			"user/logout/" => _("label_logout")
		]
	]
];
