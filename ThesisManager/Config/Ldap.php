<?php
namespace ThesisManager\Config;

/*
 *	LDAP configuration file.
 *	@version 1.0.0
 */
abstract class Ldap
{
    const Host = "192.168.40.21";
    const Port = 389;
    const Domain = "LABRANET";
	const DN = "DC=Labranet,DC=jamk,DC=fi"; // Distinguished Names
}

