<?php
namespace ThesisManager\Config;
use ThesisManager\Constant\Environment;

abstract class Application
{
    //const Environment = Environment::Production;
    const Environment = Environment::Development;
    const Encoding = "UTF-8";
    const DefaultLocale = "fi_FI";
    const TimeZone = "Europe/Helsinki";
}

