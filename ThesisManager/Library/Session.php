<?php
namespace ThesisManager\Library;
use ThesisManager\Config\ThesisDB;

// Version 1.0;

/**
 *	Custom session handler to save the sessions to a remote database for load balancing.
 */
class Session {
	private $connection;

	function __construct() {
		$this->connection = new Database(ThesisDB::Host, ThesisDB::User, ThesisDB::Password, ThesisDB::Schema);
		session_set_save_handler(
			array($this, "Open"),
			array($this, "Close"),
			array($this, "Read"),
			array($this, "Write"),
			array($this, "Destroy"),
			array($this, "GC")
		);
		ini_set("session.cookie_httponly", 1);
		session_start();
	}

	/**
	 *	Returns true if the database connection is open.
	 */
	public function Open() {
		if ($this->connection) {
			return true;
		}
		return false;
	}

	/**
	 *	Closes the database connection.
	 */
	public function Close() {
		$this->connection = null;
        return true;
	}

	/**
	 *	Fetches the session data from the database.
	 */
	public function Read($id) {
		$this->connection->Query("
            SELECT session_data
              FROM `session`
             WHERE session_id = :id
        ");
		$this->connection->Bind(":id", $id);

		if ($this->connection->Execute()) {
			$row = $this->connection->Single();
			return $row["session_data"] ?: "";
		}
		return "";
	}

	/**
	 *	Updates the session data into the database.
	 */
	public function Write($id, $data) {
		$time = time();
		$this->connection->Query("
            REPLACE
               INTO `session`
             VALUES (:id, :data, :time)
        ");
		$this->connection->Bind(":id", $id);
		$this->connection->Bind(":data", $data);
        $this->connection->Bind(":time", $time);
		if ($this->connection->Execute()) {
			return true;
		}
		return false;
	}

	/**
	 *	Deletes the session data from the database.
	 */
	public function Destroy($id) {
		$this->connection->Query("
            DELETE
              FROM `session`
             WHERE session_id = :id
        ");
		$this->connection->Bind(":id", $id);
		if ($this->connection->Execute()) {
			return true;
		}
		return false;
	}

	/**
	 *	Garbage Collection. Deletes old/expired session data from the database.
	 */
	public function GC($max) {
		$old = time() - $max;
		$this->connection->Query("
            DELETE
              FROM `session`
             WHERE session_time < :old
        ");
		$this->connection->Bind(":old", $old);
		if ($this->connection->Execute()) {
			return true;
		}
		return false;

	}
}
