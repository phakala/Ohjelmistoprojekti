<?php
namespace ThesisManager\Library;

/**
 *  Template handler.
 */
class Template
{
    private $bindings = [];
    private $viewFile;

    function __construct($viewFile = NULL) {
        if ($viewFile !== NULL) {
            if (!$this->getView($viewFile)) {
                throw new \Exception("Template file {$viewFile} not found.");
            }
        }
        $this->csrfToken = $_SESSION["CsrfToken"];
        $this->path = $_GET["path"] ?? "/";
    }

    function __get($binding) {
        return $this->bindings[$binding];
    }

    function __set($binding, $value) {
        $this->bindings[$binding] = $value;
    }

    private function getView($viewFile) {
        $filePaths = [
            "/ThesisManager/View/",
            "/ThesisManager/View/Page/",
            "/ThesisManager/View/Partial/"
        ];
        foreach($filePaths as $path) {
            $filePath = ROOT_PATH . $path . $viewFile;
            if (is_file($filePath)) {
                $this->viewFile = $filePath;
                return true;
            } elseif (is_file($filePath . ".phtml")) {
                $this->viewFile = $filePath . ".phtml";
                return true;
            }
            elseif (is_file($filePath . ".html")) {
                $this->viewFile = $filePath . ".html";
                return true;
            }
        }
        return false;
    }

    public function Display() {
        ob_start();
        extract($this->bindings, EXTR_OVERWRITE);
        require $this->viewFile;
    }

    public function ToString($extract = true) {
        ob_start();
		if ($extract) extract($this->bindings, EXTR_OVERWRITE);
        require $this->viewFile;
        return ob_get_clean();
    }
}

