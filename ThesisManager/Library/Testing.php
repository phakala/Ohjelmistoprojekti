<?php
/**
 *	Contains classes for creating unit tests.
 */

class Assert
{
    public static function AreEqual($a, $b) {
        if ($a !== $b) throw new Exception("Values are not equal.");
    }
}

class TestResult
{
    protected $exception;
    protected $success;
    protected $test;

    public function Name() {
        return $this->test->name;
    }

    public function Success() {
        return $this->success;
    }

    public function Pass(Test $object, ReflectionMethod $test) {
        $result = new self();
        $result->test = $test;
        $result->success = true;

        return $result;
    }

    public function Fail(Test $object, ReflectionMethod $test, Exception $exception) {
        $result = new self();
        $result->test = $test;
        $result->success = false;
        $result->exception = $exception;
        return $result;
    }
}

abstract class Test
{
    protected $results = [];
    private $passed = 0;
    private $failed = 0;

    public function Results() {
        return $this->results;
    }

    protected function Log(TestResult $result) {
        $this->results[] = $result;
        $success = $result->Success() ? "PASS" : "FAIL";
        printf(" Test: %s %s\n", $result->Name(), $success);
    }

    final public function RunTests() {
        $class = new \ReflectionClass($this);
        printf(" Running %s tests:\n\n", $class->name);
        foreach($class->GetMethods() as $method) {
            if ($method->class !== "Test") {
                try {
                    call_user_func($method->class ."::". $method->name);
                    $result = TestResult::Pass($this, $method);
                    $this->passed++;
                }
                catch (Exception $e) {
                    $result = TestResult::Fail($this, $method, $e);
                    $this->failed++;
                }
                $this->Log($result);
            }
        }
        $total = $this->passed + $this->failed;
        printf("\n Tests finished. Total %s test(s) ran.\n %s test(s) failed.\n\n", $total, $this->failed);
    }
}
