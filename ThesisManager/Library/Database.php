<?php
namespace ThesisManager\Library;
use PDO;

/**
 *  MySQL database class.
 */
class Database extends PDO {
	private $db;
	private $conn;
	private $error;

	function __construct($host, $user, $pswd, $dbname) {
		try {
			$this->db = new PDO("mysql:host={$host};dbname={$dbname};charset=utf8", $user, $pswd);
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		}
		catch(PDOException $e) {
			$this->error = $e->getMessage();
			die($this->error);
		}
	}

	public function Query($query) {
		$this->conn = null;
		$this->conn = $this->db->prepare($query);
	}

	public function Bind($param, $value, $type = null) {
		if (is_null($type)) {
			switch (true) {
				case is_int($value):
					$type = PDO::PARAM_INT;
					break;
				case is_bool($value):
					$type = PDO::PARAM_BOOL;
					break;
				case is_null($value):
					$type = PDO::PARAM_NULL;
					break;
				default:
					$type = PDO::PARAM_STR;
			}
		}
		$this->conn->bindValue($param, $value, $type);
	}

	public function Execute() {
		return $this->conn->execute();
	}

	public function All() {
		$this->Execute();
		return $this->conn->fetchAll(PDO::FETCH_ASSOC);
	}

	public function Single() {
		$this->Execute();
		return $this->conn->fetch(PDO::FETCH_ASSOC);
	}

	public function RowCount() {
		return $this->conn->rowCount();
	}

	public function LastInsertId($name = NULL) {
		return $this->db->lastInsertId();
	}

	public function BeginTransaction() {
		return $this->db->beginTransaction();
	}

	public function EndTransaction() {
		return $this->db->commit();
	}

	public function CancelTransaction() {
		return $this->db->rollBack();
	}

	public function DebugDumpParams() {
		return $this->conn->debugDumpParams();
	}
}
