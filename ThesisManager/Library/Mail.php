<?php
namespace ThesisManager\Library;
use ThesisManager\Config\ThesisDB;
use ThesisManager\Config\Smtp;

// Version 1.0;

/**
 *	SMTP Mail library.
 */
class Mail {
	private $connection;

	function __construct() {
		$this->connection = new Database(ThesisDB::Host, ThesisDB::User, ThesisDB::Password, ThesisDB::Schema);
	}

	public function Send($to, $subject, $message) {
		$headers =	"From: " . Smtp::FromAddress . "\r\n" .
           			"Reply-To: " . Smtp::ReplyAddress . "\r\n" .
					"X-Mailer: PHP/" . phpversion();
		if (mail($to, $subject, $message, $headers)) {
			return true;
		}
		return false;
	}

}
