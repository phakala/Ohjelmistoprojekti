<?php
namespace ThesisManager\Library;
use Locale;

/**
 *  Internalization class.
 */
class i18n {

    // Source: http://www.thefutureoftheweb.com/blog/use-accept-language-header
    public static function GetPreferredLocale() {
        $locales = [];
        if (isset($_SERVER["HTTP_ACCEPT_LANGUAGE"])) {
            // Break up string into pieces (languages and q factors)
            preg_match_all("/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i", $_SERVER["HTTP_ACCEPT_LANGUAGE"], $array);
            // Check matches
            if (count($array[1])) {
                // Create a list locale => priority
                $locales = array_combine($array[1], $array[4]);
                // Set default to 1 for languages without a priority factor
                foreach ($locales as $locale => $priority) {
                    if ($priority === '') $locales[$locale] = 1;
                }
                // Sort list based on value
                arsort($locales, SORT_NUMERIC);
            }
        }
        $array = [];
        foreach ($locales as $locale => $priority) {
            if (strpos($locale, '-') !== false) {
                $locale = explode('-', $locale);
                $locale[1] = mb_strtoupper($locale[1]);
                $locale = implode('_', $locale);
            }
            $array[$locale] = $priority;
        }
        $locales = $array;
        array_unique($locales);
        foreach ($locales AS $locale => $priority) {
            $array = glob(ROOT_PATH . "\ThesisManager\Locale\\" . $locale . '*');
            if (!empty($array[0])) {
                $array = array_reverse(explode(DIRECTORY_SEPARATOR, $array[0]));
                $locale = $array[0];
                return $locale;
            }
        }
        return false;
    }

}
