<?php
/**
 *  All requests are directed through this file.
 *  Contains basic application setup and MVC routing.
 */

namespace ThesisManager;
use ThesisManager\Config\Application;
use ThesisManager\Constant\Environment;
use ThesisManager\Library\Session;
use ThesisManager\Library\i18n;


// Define the root folder path and the application url. The public folder path was already defined in Public/index.php.
define("ROOT_PATH", dirname(__DIR__));
define("APP_URL", $_SERVER["HTTP_HOST"]);

// Define php error log file.
ini_set("error_log", ROOT_PATH . "/ThesisManager/Logs/php-errors.log");

// Register an autoloader for our classes.
spl_autoload_register(function ($className) {
    $className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
    //echo ROOT_PATH . DIRECTORY_SEPARATOR . $className . ".php<br />";
    if (is_file(ROOT_PATH . DIRECTORY_SEPARATOR . $className . ".php")) {
        require(ROOT_PATH . DIRECTORY_SEPARATOR . $className . ".php");
    }
});

// Handle errors and exceptions.
set_exception_handler(function (\Throwable $t) {
    $errorMsg = get_class($t) . ": " . $t->getMessage() . " in " . $t->getFile() . " on line " . $t->getLine() . ".\n" . $t->getTraceAsString() . ".\n";
    if (APP_ENV == Environment::Development) {
        echo "<pre>{$errorMsg}</pre>";
    } else {
        // TODO error template.
    }
    error_log($errorMsg);
});

// Define the application environment.
define("APP_ENV", Application::Environment);

// Set the encoding and the timezone.
mb_internal_encoding(Application::Encoding);
date_default_timezone_set(Application::TimeZone);

// Start output buffering and a session.
ob_start();
$session = new Session;

// Set the locale.
if (!isset($_SESSION["Locale"])) {
    if ($locale = i18n::GetPreferredLocale()) {
        $_SESSION["Locale"] = $locale;
    }
    else {
        $_SESSION["Locale"] = Application::DefaultLocale;
    }
}

putenv("LC_ALL=" . $_SESSION["Locale"]);
setlocale(LC_ALL, $_SESSION["Locale"]);
$domain = $_SESSION["Locale"];
bindtextdomain($domain, ROOT_PATH . "/ThesisManager/Locale/");
bind_textdomain_codeset($domain, Application::Encoding);
textdomain($domain);

// Create a token against CSRF attacks.
$_SESSION["CsrfToken"] = $_SESSION["CsrfToken"] ?? bin2hex(random_bytes(32));

// Get the requested url path.
$path = empty($_GET["path"]) ? "/" : htmlspecialchars(strtolower($_GET["path"]));

// Get possible routes for controller/action routing.
$routes = require "Config/Routes.php";

// Route the request.
foreach($routes as $route => $routeTo) {
    if (preg_match($route, $path, $matches)) {
        // Get controller and action.
        $controller = "ThesisManager\Controller\\" . $routeTo[0];
        $controller = new $controller;
        $action = $routeTo[1];
        // Remove the first match (controller/action part) and pass possible blocks to the action.
        array_shift($matches);
        $controller->{$action}($matches);
        break;
    }
}

