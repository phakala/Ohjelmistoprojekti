<?php
namespace ThesisManager;
use ThesisManager\Library\Database;

/**
 *  The base class for a model in the MVC architecture.
 */
abstract class Model {
    protected $db;

    function __construct(Database $db) {
        $this->db = $db;
    }
}
