<?php
namespace ThesisManager;
use ThesisManager\Controller\Error;

/**
 *  The base class for a controller in the MVC architecture.
 */
abstract class Controller {

    /**
     *  Triggered when a controller tries to call an inaccessible method.
     */
	public function __call($method, $args) {
        if (!method_exists($this, $method)) {
            throw new \Exception("Method " . $method . " doesn't exist in " . get_class($this) . ".");
        }
    }

	/**
	 *	Forwards a request to a different controller.
	 */
	public function Forward($controller, $action, $matches = NULL) {
		// Clear all output buffers.
        while (ob_get_level()) ob_get_clean();
		$controller = "ThesisManager\Controller\\" . $controller;
        $controller = new $controller;
        $controller->{$action}($matches);
		exit;
	}

    /**
     *  Returns true if a given CSRF token matches the session CSRF token.
     *  @param string $token
     *  @return boolean
     */
    public function CheckCsrfToken($token = null) {
        if ($token === null && !isset($_SERVER["HTTP_X_CSRF_TOKEN"])) {
            return false;
        }
        $token = $token ?? $_SERVER["HTTP_X_CSRF_TOKEN"];
        if (!empty($token)) {
            if (hash_equals($token, $_SESSION["CsrfToken"])) {
                return true;
            }
            else {
                // TODO CSRF attack logging?
            }
        }
        return false;
    }

    /**
     *  Displays a 404 page through the Error controller.
     */
    public function DisplayNotFound() {
        // Clear all output buffers.
        while (ob_get_level()) ob_get_clean();
        // Display 404 error.
        $error = new Error;
        $error->NotFound();
    }

    /**
     *  Returns a prefixed JSON string for security.
     *  https://docs.angularjs.org/api/ng/service/$http#json-vulnerability-protection
     */
    public function SafeJson($data) {
        return ")]}',\n" . json_encode($data);
    }

	/*
	 *	Sanitizes input passed by reference.
	 */
	public function Sanitize(&$value, &$key) {
		$key = htmlspecialchars(trim($key));
		$value = htmlspecialchars(trim($value));
	}

    /**
     *  Returns sanitized JSON encoded POST data.
     */
    public function GetJsonPostData() {
        $data = json_decode(file_get_contents("php://input"), true);
        array_walk_recursive($data, [$this, 'Sanitize']);
		return $data;
    }

    /**
     *  Removes superscript symbols two levels and above. Prevents "zalgo text".
     *  Taken from https://stackoverflow.com/questions/32921751/how-to-prevent-zalgo-text-using-php
     */
    public function FilterString($string) {
        return preg_replace("~(?:[\p{M}]{1})([\p{M}])+?~uis","", $string);
    }

}

