#!/usr/bin/php

<?php
namespace ThesisManager;

// Register an autoloader.
spl_autoload_register(function ($className) {
    if (is_file("../UnitTest/" . $className . ".php")) {
        require("../UnitTest/" . $className . ".php");
    }
});

$example = new Example();
$example->RunTests();
