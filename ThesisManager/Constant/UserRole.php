<?php
namespace ThesisManager\Constant;

abstract class UserRole {
    const Admin = 1;
    const Student = 2;
    const ThesisSupervisor = 4;
    const LanguageInstructor = 8;
	const EducationCoordinator = 16;
	const EducationDirector = 32;
}
