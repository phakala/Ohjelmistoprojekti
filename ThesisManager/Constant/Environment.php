<?php
namespace ThesisManager\Constant;

abstract class Environment {
    const Development = 1;
    const Production = 2;
}
