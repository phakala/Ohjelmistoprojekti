/*global
    angular
*/

angular
    .module('app')
    .factory('userService', ['$http', userService]);

function userService($http) {
    return {
        getAll: getAll,
		getSupervisors: getSupervisors,
		save: save
    };

    function getAll() {
        return $http.get('/api/users/get/');
    }

	function getSupervisors() {
        return $http.get('/api/users/get/supervisors/');
    }

	function save(user) {
        var data = {
			user: user
        };
        return $http.post('/api/users/save/', data);
    }
}
