/*global
    alert, angular, console, document
*/

angular
    .module('app')
    .controller('AuthenticationController', ['loginService', '$window', AuthenticationController]);

function AuthenticationController(loginService, $window) {
    'use strict';
    var vm = this;
    vm.clicked = false;
    vm.messages = {};
    vm.messages.login = {};
    vm.login = login;

    function login() {
        vm.clicked = true;
        vm.messages.login.error = "asd";
        vm.messages.login = {};
        return loginService.login(vm.login.email, vm.login.password)
            .then(function(response) {
                $window.location.reload();
            })
            .catch(function(response) {
                console.log(response.data);
                vm.clicked = false;
                vm.messages.login = response.data;
            });
    }
}
