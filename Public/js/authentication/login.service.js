/*global
    angular
*/

angular
    .module('app')
    .factory('loginService', ['$http', loginService]);

function loginService($http) {
    return {
        login: login
    };

    function login(email, password) {
        var data = {
            email: email,
            password: password
        };
        return $http.post('/user/login/', data);
    }
}
