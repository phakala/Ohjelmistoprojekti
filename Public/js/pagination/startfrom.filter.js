/*global
    angular
*/

angular
    .module('app')
    .filter('startFrom', startFrom);

function startFrom($filter) {
    'use strict';

    return {
        startFrom: startFrom
    };

    function startFrom(input, start) {
        start = +start;
        return input.slice(start);
    }
}
