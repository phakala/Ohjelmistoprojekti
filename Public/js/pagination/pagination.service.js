/*global
    angular
*/

angular
    .module('app')
    .factory('paginationService', ['$filter', paginationService]);

function paginationService($filter) {
	var sv = this;
	sv.currentPage = 1;
	sv.pageSize = 5;
	sv.range = range;

	return {
		getPagination: getPagination
	};

	function getPagination(currentPage, pageSize, totalItems) {
		sv.currentPage = currentPage || sv.currentPage;
		sv.pageSize = pageSize || sv.pageSize;
		var totalPages = Math.ceil(totalItems / pageSize);
		var startPage, endPage;
		if (totalPages <= 10) {
			startPage = 1;
			endPage = totalPages;
		}
		else {
			if (currentPage <= 6) {
				startPage = 1;
				endPage = 10;
			} else if (currentPage + 4 >= totalPages) {
				startPage = totalPages - 9;
				endPage = totalPages;
			} else {
				startPage = currentPage - 5;
				endPage = currentPage + 4;
			}
		}
		var startIndex = (currentPage - 1) * pageSize;
		var endIndex = Math.min(startIndex + pageSize - 1, totalItems -1);
		var pages = sv.range(startPage, endPage);
		return {
			totalItems: totalItems,
			currentPage: currentPage,
			pageSize: pageSize,
			totalPages: totalPages,
			startPage: startPage,
			endPage: endPage,
			startIndex: startIndex,
			endIndex: endIndex,
			pages: pages
		};
	}

	function range(lowEnd, highEnd){
		var arr = [],
		c = highEnd - lowEnd + 1;
		while ( c-- ) {
			arr[c] = highEnd--;
		}
		return arr;
	}
}
