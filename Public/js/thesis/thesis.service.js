/*global
    angular, console
*/

angular
    .module('app')
    .factory('thesisService', ['$http', thesisService]);

function thesisService($http) {
    return {
        getAll: getAll,
		getVersions: getVersions,
		save: save,
		saveVersion: saveVersion
    };

    function getAll() {
        return $http.get('/api/theses/get/');
    }

	function getVersions() {
        return $http.get('/api/versions/get/');
    }

	function save(thesis) {
        var data = {
			thesis: thesis
        };
        return $http.post('/api/theses/save/', data);
    }

	function saveVersion(version) {
		var data = {
			version: version
		};
		console.log(version);
		return $http.post('/api/versions/save/version/', data);
	}
}
