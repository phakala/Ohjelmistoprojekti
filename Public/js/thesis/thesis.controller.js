/*global
    alert, angular, console, document
*/

angular
    .module('app')
    .controller('ThesisController', ['thesisService', '$window', ThesisController]);

function ThesisController(thesisService, $window) {
    'use strict';
    var vm = this;
    vm.clicked = false;
	vm.saved = false;
	vm.clientName = '';
	vm.clientEmail = '';
	vm.topic = '';
	vm.description = '';
	vm.messages = {
		error: {}
	};

    vm.save = save;

    function save() {
        vm.clicked = true;
		vm.saved = false;
        vm.messages.error = {};
        return thesisService.save(vm.clientName, vm.clientEmail, vm.topic, vm.description)
            .then(function(response) {
				vm.saved = true;
            })
            .catch(function(response) {
                vm.messages.error = response.data;
				console.log(vm.messages.error);
            })
			.finally(function() {
				vm.clicked = false;
			});
    }
}
