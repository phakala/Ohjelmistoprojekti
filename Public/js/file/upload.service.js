/*global
    alert, angular, console, document
*/

angular
    .module('app')
    .service('uploadService', ['$http', uploadService]);

function uploadService($http) {
	return {
		upload: upload
	};

	function upload(data, callback) {
		console.log(data);
		return $http({
			url: '/api/files/upload/',
			method: 'POST',
			data: data,
			headers: {'Content-Type': undefined}
		});
	}
}
