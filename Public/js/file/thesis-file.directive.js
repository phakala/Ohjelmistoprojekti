/*global
    alert, angular, console, FormData
*/

angular
    .module('app')
    .directive('thesisFile', ['uploadService', thesisFile]);

function thesisFile(uploadService) {
	return {
		restrict: 'A',
		scope: {
			selectedVersion: '='
		},
		link: link
	};

	function link($scope, element, attr) {
		element.bind('change', function() {
			var formData = new FormData();
			console.log($scope.selectedVersion);
			for (var i = 0; i < element[0].files.length; i++) {
				formData.append('files[]', element[0].files[i]);
			}
			formData.append('versionId', $scope.selectedVersion.versionId);
			return uploadService.upload(formData)
				.then(function (response) {
					console.log(response.data);
					for (var i = 0; i < response.data.length; i++) {
						$scope.selectedVersion.files.push({
							name: response.data[i].name
						});
					}
				});
		});
	}
}
