/*global
   angular, document
*/

angular
    .module('app')
    .config(config);

function config($httpProvider) {
    'use strict';
    $httpProvider.defaults.headers.common['X-CSRF-Token'] = document.getElementsByName('csrf-token')[0].getAttribute('content');
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8;';
}
