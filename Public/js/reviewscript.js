$(document).ready(function() {
	$('#counter').hide();
	$('#reviewSubmit').prop('disabled', true);
	$('#reviewSubmit').css('background-color', 'red');
	$("input[type=radio]").hide();
    $('input:radio').change(function() {

    });
	$('.review_column').click(function(){
		$(this).find('input:radio').prop("checked", true);
		$('#counter').show();
		var rows = [];
		var value = parseInt($(this).val());
		$('input[type="radio"]').each(function(){
			$(this).parent().css({"background-color": "white"});
		});
		$('input[type="radio"]:checked').each(function(){
			rows.push(parseInt($(this).val()));
			$(this).parent().css({"background-color": "whitesmoke"});
		});
		var checkedradios = $('input:radio:checked').length;
		if (checkedradios == 16) {
			$('#reviewSubmit').prop('disabled', false);
			$('#reviewSubmit').css('background-color', '#3273dc');
			$('#formerror').hide();
		}
		$("#counter").show();
		countAverage(rows, checkedradios);
	});
	function countAverage(rows, checked) {
		var sum = 0;
		for (var i=0; i<rows.length; i++) {
			sum = sum + rows[i];
		}
		var average = Math.round((sum / checked) * 100) / 100;
		document.getElementById("average").innerHTML = average;
	}
});
