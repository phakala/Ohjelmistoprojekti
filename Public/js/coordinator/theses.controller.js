/*global
    alert, angular, console, document
*/

angular
    .module('app')
    .controller('CoordinatorController', ['thesisService', 'userService', 'paginationService', '$filter', '$window', CoordinatorController]);

function CoordinatorController(thesisService, userService, paginationService, $filter, $window) {
    'use strict';
    var vm = this;
	vm.clicked = false;
	vm.saved = false;
	vm.selectedThesis = -1;

	vm.theses = [];
	vm.thesesList = [];
	vm.supervisors = [];
	vm.supervisorsList = [];

	vm.perPage = 15;
	vm.p = {};
	vm.q = '';

	vm.getTheses = getTheses;
	vm.setPage = setPage;
	vm.saveThesis = saveThesis;

    getTheses();
	getSupervisors();

    function getTheses() {
        return thesisService.getAll()
            .then(function(response) {
                vm.theses = response.data;
				setPage(1);
            })
            .catch(function(response) {
                // TODO error msg
                console.log(response);
            });
    }

	function getSupervisors() {
        return userService.getSupervisors()
            .then(function(response) {
                vm.supervisors = response.data;
				console.log(vm.supervisors);
            })
            .catch(function(response) {
                // TODO error msg
                console.log(response);
            });
    }

	function saveThesis() {
		vm.clicked = true;
		vm.saved = false;
        return thesisService.save(vm.thesesList[vm.selectedThesis])
            .then(function(response) {
				vm.saved = true;
            })
            .catch(function(response) {
				console.log(response.data);
            })
			.finally(function() {
				vm.clicked = false;
			});
	}

	function setPage(page) {
		if (page < 1 || page > vm.p.totalPages) {
			return;
		}
		vm.p = paginationService.getPagination(page, vm.perPage, vm.theses.length);
		vm.thesesList = vm.theses.slice(vm.p.startIndex, vm.p.endIndex + 1);
	}

	function filterUsers() {
		return $filter('filter')(vm.theses, vm.q);
	}
}
