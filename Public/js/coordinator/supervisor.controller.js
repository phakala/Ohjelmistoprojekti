/*global
    alert, angular, console, document
*/

angular
    .module('app')
    .controller('CoordinatorController', ['userService', 'paginationService', '$filter', '$window', CoordinatorController]);

function CoordinatorController(userService, paginationService, $filter, $window) {
    'use strict';
    var vm = this;
	vm.clicked = false;
	vm.saved = false;
	vm.selectedSupervisor = -1;

	vm.supervisors = [];
	vm.supervisorsList = [];

	vm.perPage = 15;
	vm.p = {};
	vm.q = '';

	vm.setPage = setPage;
	vm.saveSupervisor = saveSupervisor;

	getSupervisors();


	function getSupervisors() {
        return userService.getSupervisors()
            .then(function(response) {
                vm.supervisors = response.data;
				setPage(1);
            })
            .catch(function(response) {
                // TODO error msg
                console.log(response);
            });
    }

	function saveSupervisor() {
		vm.clicked = true;
		vm.saved = false;
        return userService.save(vm.supervisorsList[vm.selectedSupervisor])
            .then(function(response) {
				vm.saved = true;
            })
            .catch(function(response) {
				console.log(response.data);
            })
			.finally(function() {
				vm.clicked = false;
			});
	}

	function setPage(page) {
		if (page < 1 || page > vm.p.totalPages) {
			return;
		}
		vm.p = paginationService.getPagination(page, vm.perPage, vm.supervisors.length);
		vm.supervisorsList = vm.supervisors.slice(vm.p.startIndex, vm.p.endIndex + 1);
	}

	function filterUsers() {
		return $filter('filter')(vm.supervisors, vm.q);
	}
}
