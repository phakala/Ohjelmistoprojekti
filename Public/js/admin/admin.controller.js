/*global
    alert, angular, console, document
*/

angular
    .module('app')
    .controller('AdminController', ['userService', 'paginationService', '$filter', '$window', AdminController]);

function AdminController(userService, paginationService, $filter, $window) {
    'use strict';
    var vm = this;
	vm.selectedUser = -1;
	vm.clicked = false;
	vm.saved = false;

	vm.users = [];
	vm.userList = [];

	vm.perPage = 15;
	vm.p = {};
	vm.q = '';

	vm.getUsers = getUsers;
	vm.setPage = setPage;
	vm.saveUser = saveUser;

    getUsers();

    function getUsers() {
        return userService.getAll()
            .then(function(response) {
                vm.users = response.data;
				setPage(1);
            })
            .catch(function(response) {
                // TODO error msg
                console.log(response);
            });
    }

	function saveUser() {
		vm.clicked = true;
		vm.saved = false;
        return userService.save(vm.userList[vm.selectedUser])
            .then(function(response) {
				vm.saved = true;
            })
            .catch(function(response) {
				console.log(response.data);
            })
			.finally(function() {
				vm.clicked = false;
			});
	}

	function setPage(page) {
		if (page < 1 || page > vm.p.totalPages) {
			return;
		}
		console.log(filterUsers().length);
		vm.p = paginationService.getPagination(page, vm.perPage, vm.users.length);
		vm.userList = vm.users.slice(vm.p.startIndex, vm.p.endIndex + 1);
	}

	function filterUsers() {
		return $filter('filter')(vm.users, vm.q);
	}
}
