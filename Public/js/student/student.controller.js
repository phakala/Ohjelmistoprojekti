/*global
    alert, angular, console, document
*/

angular
    .module('app')
    .controller('StudentController', ['thesisService', '$window', StudentController]);

function StudentController(thesisService, $window) {
    'use strict';
    var vm = this;
    vm.clicked = false;
	vm.formOpen = false;
	vm.saved = false;

	vm.selectedVersion = -1;

	vm.clientName = '';
	vm.clientEmail = '';
	vm.topic = '';
	vm.description = '';

	vm.messages = {
		error: {}
	};

	vm.emptyVersion = {
		title: null,
		comment: null,
		ready: 0
	};

	vm.versions = [];
	vm.files = [];

    vm.saveSuggestion = saveSuggestion;
	vm.saveVersion = saveVersion;

	getVersions();

    function saveSuggestion() {
        vm.clicked = true;
		vm.saved = false;
        vm.messages.error = {};
		var thesis = {
			clientName: vm.clientName,
			clientEmail: vm.clientEmail,
			topic: vm.topic,
			description: vm.description
		};
        return thesisService.save(thesis)
            .then(function(response) {
				vm.saved = true;
				console.log(response.data);
            })
            .catch(function(response) {
                vm.messages.error = response.data;
				console.log(vm.messages.error);
            })
			.finally(function() {
				vm.clicked = false;
			});
    }

	function getVersions() {
		return thesisService.getVersions()
			.then(function(response) {
				vm.versions = response.data;
				vm.versions.push(vm.emptyVersion);
				console.log(response.data);
			})
			.catch(function(response) {
				// TODO error msg
				console.log(response);
		});
	}

	function saveVersion() {
		vm.clicked = true;
		vm.saved = false;
		vm.messages.error = {};
		return thesisService.saveVersion(vm.versions[vm.selectedVersion])
			.then(function(response) {
				vm.saved = true;
				if (vm.versions[vm.selectedVersion] == vm.versions[vm.versions.length - 1]) {
					vm.versions.push(vm.emptyVersion);
				}
				console.log(response.data);
			})
			.catch(function(response) {
                vm.messages.error = response.data;
				console.log(vm.messages.error);
            })
			.finally(function() {
				vm.clicked = false;
			});
	}
}
